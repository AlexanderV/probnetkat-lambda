%%% -*- mode: prolog -*-

% treat failed filters as failure.
 
% drop(_,[]).
drop(_) :- fail.

skip(PH).

test(Field,Value,[P|PH]) :- member(Field-Value,P).
% test(Field,Value,[P|PH],[]) :- \+member(Field-Value,P).

member(X,[X|_]).
member(X,[Y|Ys]) :- X \== Y, member(X,Ys).

modify(Field,Value,[P|PH],[NewP|PH]) :-
    modify_packet(Field,Value,P,NewP).

modify_packet(Field,Value,[],[Field-Value]).
modify_packet(Field,Value,[F-V|Packet],[F-Value|Packet]) :-
    Field == F.
modify_packet(Field,Value,[F-V|Packet],[F-V|Packet2]) :-
    Field \== F,
    modify_packet(Field,Value,Packet,Packet2).

dup([P|PH],[P,P|PH]).

{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

module ProbNetKat.Semantics.CompilationSpec where

import           Test.Hspec
import           Test.QuickCheck (property, (==>))

import           ProbNetKat.Result
import           ProbNetKat.Semantics.Compilation as C
import           ProbNetKat.Semantics.Compilation.Typing as CT
import           ProbNetKat.Syntax as Syntax
import           ProbNetKat.Syntax.DeBruijn as DB
import qualified ProbNetKat.Syntax.Simple as S
import           ProbNetKat.Syntax.Simple.Arbitrary (Name)
import           ProbNetKat.Typing as T
import           ProbNetKat.Typing.Reconstruction as R

import           ProbNetKat.TypingSpec (successShouldBe, successShouldSatisfy)

isTerminal
    :: (Syntax.XExpression ext var lvar ~ S.XClosed var lvar)
    => Syntax.Expression ext var lvar -> Bool
isTerminal e = case e of
  Syntax.Duplication{} -> True
  Syntax.Filter{} -> True
  Syntax.Modification{} -> True
  Syntax.Parallel _ e1 e2 -> isTerminal e1 && isTerminal e2
  Syntax.Sequential _ e1 e2 -> isTerminal e1 && isTerminal e2
  Syntax.Choice _ e1 _ e2 -> isTerminal e1 && isTerminal e2
  Syntax.Iteration _ e' -> isTerminal e'
  Syntax.Lambda{} -> True
  Syntax.Application{} -> False
  Syntax.Force{} -> False
  Syntax.Produce{} -> True
  Syntax.To{} -> False
  Syntax.Expression x -> S.absurdX x

simplify
  :: (Syntax.XExpression ext var lvar ~ S.XClosed var lvar)
  => Syntax.Expression ext var lvar -> S.Expression var lvar
simplify = S.simplify S.absurdX

spec :: Spec
spec = do
  describe "reduction" $ do
    it "returns a terminal" $ property $
      \(e :: T.Expression Name Name) ->
      isSuccess (CT.check mempty e) ==>
      let db = DB.addDeBruijn mempty (S.simplify S.absurdX e)
      in reduce db `successShouldSatisfy` (isTerminal . S.simplify S.absurdX)

    it "reflects terminals" $ property $
      \(e :: T.Expression Name Name) ->
      isSuccess (CT.check mempty e) && isTerminal (simplify e) ==>
      let db = DB.addDeBruijn mempty (simplify e)
      in fmap simplify (reduce db) `successShouldBe` simplify e
  
    it "preserves types (preservation)" $ property $
      \(e :: T.Expression Name Name) ->
      isSuccess (CT.check mempty e) ==>
      let db = DB.addDeBruijn mempty (S.simplify S.absurdX e)
          preservation e' = isSuccess $
            R.typeExpression (S.simplify S.absurdX e') >>= CT.check mempty
      in reduce db `successShouldSatisfy` preservation

  describe "erasure" $ do
    it "is complete on well-typed terminals" $ property $
      \(e :: T.Expression Name Name) -> isTerminal e ==>
      let e' = simplify e
      in erase (DB.addDeBruijn (Syntax.freevars e') e')
         `shouldSatisfy` isSuccess

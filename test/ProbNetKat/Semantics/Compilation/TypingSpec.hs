{-# LANGUAGE ScopedTypeVariables #-}

module ProbNetKat.Semantics.Compilation.TypingSpec where

import           Test.Hspec
import           Test.QuickCheck (property, (==>), forAll, vectorOf)

import           ProbNetKat.Result
import qualified ProbNetKat.Semantics.Compilation.Typing as CT
import qualified ProbNetKat.Syntax.Simple as S
import qualified ProbNetKat.Syntax as S
import           ProbNetKat.Syntax.Simple.Arbitrary (Name)
import qualified ProbNetKat.Typing as T
import           ProbNetKat.Typing.Arbitrary (genComputation,ExprConf(..))
import qualified ProbNetKat.Typing.Reconstruction as R
import qualified ProbNetKat.TypingSpec as TSpec
import           Text.Pretty

ctCheck :: (Eq var, Pretty var) => T.Env var -> T.Expression var var -> Bool
ctCheck tenv expr = isSuccess (CT.check tenv expr)

spec :: Spec
spec =
  describe "compilation type checking" $ do
    it "rejects ill-scoped expressions" $ property $
      \(MkExprConf _ expr :: ExprConf Name) ->
        not (null (S.freevars expr :: [Name]))
        ==> CT.check mempty expr `shouldSatisfy` isFailure

    it "preserves the type of well-typed expressions" $ property $
      \(texpr :: T.Expression Name Name) ->
        let result = CT.check mempty texpr
            Success t = result
        in isSuccess result ==> t == T.typeOf texpr

    it "admits parallel choice only if unit producing" $
      let term = T.parallel (T.produce T.unit) (T.produce T.unit)
      in CT.check (mempty :: T.Env String) term `shouldSatisfy` isSuccess

    it "admits parallel choice if unit producing" $ property $ \tenv ->
      forAll (genComputation tenv (T.Producer T.Unit)) $
      \(e' :: T.Expression Name Name) ->
        let e = S.Parallel (T.HType (T.Producer T.Unit)) e' e'
        in ctCheck mempty e' ==> CT.check tenv e `shouldSatisfy` isSuccess


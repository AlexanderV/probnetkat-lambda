{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module ProbNetKat.Typing.Arbitrary where

import Test.QuickCheck (
  Arbitrary(arbitrary,shrink), genericShrink, oneof, elements, sized, resize,
  getNonNegative)
import           Test.QuickCheck.Gen
import           Data.Function (on)
import           Data.List (nubBy)
import           Data.Coerce

import           ProbNetKat.Syntax.Simple.Arbitrary (getLabel, getName)

import qualified ProbNetKat.Syntax as S
import           ProbNetKat.Syntax (
  Predicate(Skip,Drop,Negation,Test, Conjunction,Disjunction))

import qualified ProbNetKat.Typing as T

-- | Configurations are a value and a typing environment under which it is
-- well-typed.
--
-- Useful for generating non-closed values.
data ValConf var = MkValConf {
  v_conf_environment :: T.Env var,
  v_conf_value       :: T.Value var var
} deriving Show

-- | Configurations consist of a computation and a typing environment under
-- which it is well-typed.
--
-- Useful for generating non-closed computations.
data ExprConf var = MkExprConf {
  e_conf_environment :: T.Env var,
  e_conf_computation :: T.Expression var var
} deriving Show

halfsize, smaller :: Gen a -> Gen a
halfsize = scale (`div` 2)
smaller = scale (\x -> max 0 (x - 1))

instance Arbitrary T.ValueType where
  arbitrary = sized valueType where
    valueType 0 = elements [T.Unit, T.Field, T.Literal]
    valueType n = oneof [
      return T.Unit,
      return T.Field,
      return T.Literal,
      fmap T.Thunk (smaller arbitrary)]
  shrink = genericShrink

instance Arbitrary T.ComputationType where
  arbitrary = sized computationType where
    computationType 0 = elements [T.Producer T.Unit]
    computationType n = oneof [
       smaller $ T.Producer <$> arbitrary,
       halfsize $ T.Function <$> arbitrary <*> arbitrary]
  shrink = genericShrink

instance Arbitrary var => Arbitrary (T.Env var) where
  arbitrary = T.Env <$> arbitrary
  shrink (T.Env env) = map T.Env (shrink env)

instance (Eq var, Arbitrary var) => Arbitrary (ValConf var) where
  arbitrary = do
    valueType <- arbitrary
    env <- arbitrary
    value <- genValue env valueType
    return (MkValConf env value)

instance (Eq var, Arbitrary var) => Arbitrary (ExprConf var) where
  arbitrary = do
    computationType <- arbitrary
    env <- arbitrary
    computation <- genComputation env computationType
    return (MkExprConf env computation)

instance (Eq var, Arbitrary var) => Arbitrary (T.Value var var) where
  arbitrary = arbitrary >>= genValue mempty

instance (Eq var, Arbitrary var) => Arbitrary (T.Expression var var) where
  arbitrary = arbitrary >>= genComputation mempty

genValue
  :: forall var. (Eq var, Arbitrary var)
  => T.Env var -> T.ValueType -> Gen (T.Value var var)
genValue tenv@(T.Env env) vt = do
  let uniqueEnv = nubBy ((==) `on` fst) env
      value :: T.ValueType -> Gen (T.Value var var)
      value T.Unit    = return T.unit
      value T.Field   = (T.label . getLabel) <$> arbitrary
      value T.Literal = (T.literal . getNonNegative) <$> arbitrary
      value (T.Thunk ct) =
        smaller $ T.thunk <$> genComputation tenv ct
  oneof $
    [value vt]
    ++
    [return (T.variable vt x) | (x,vt') <- uniqueEnv, vt == vt']

genPredicate
  :: (Eq var, Arbitrary var)
  => T.Env var -> Gen (Predicate (T.Value var var))
genPredicate tenv = sized predicate' where
  predicate' 0     = elements [Skip, Drop]
  predicate' _size = oneof [
    return Skip,
    return Drop,
    halfsize (Test <$> genValue tenv T.Field <*> genValue tenv T.Literal),
    scale (\x -> x - 1) (Negation <$> genPredicate tenv),
    halfsize (Conjunction <$> genPredicate tenv <*> genPredicate tenv),
    halfsize (Disjunction <$> genPredicate tenv <*> genPredicate tenv)]

genComputation
  :: (Eq var, Arbitrary var)
  => T.Env var -> T.ComputationType -> Gen (T.Expression var var)
genComputation tenv ct =
  let onlyUnitProduce 0 = [return T.duplication]
      onlyUnitProduce _size = [
        return T.duplication,
        smaller $ T.filter <$> genPredicate tenv,
        halfsize $
          T.modification <$> genValue tenv T.Field <*> genValue tenv T.Literal,
        smaller $ T.iteration_ <$> genComputation tenv ct]
      onlyProduce _size =
        let T.Producer vt = ct
        in [smaller $ T.produce <$> genValue tenv vt]
      onlyFunction _size =
        let T.Function vt ct' = ct        
        in [do var <- arbitrary
               T.lambda vt var <$> genComputation (T.extend var vt tenv) ct']
      other 0 = []
      other _size = [
        halfsize $
          T.sequential <$> arbitrary <*> genComputation tenv ct,
        halfsize $
          T.parallel
            <$> genComputation tenv ct <*> genComputation tenv ct,
        halfsize $
          T.choice
            <$> genComputation tenv ct
            <*> choose (0,1)
            <*> genComputation tenv ct,
        halfsize $ do
            vtArg <- arbitrary
            let funt = T.Function vtArg ct
            T.application_
              <$> genComputation tenv funt
              <*> genValue tenv vtArg,
        smaller $ T.force_ <$> genValue tenv (T.Thunk ct),
        halfsize $ do
            vtArg <- arbitrary
            var <- arbitrary
            let producert =  T.Producer vtArg
            T.to
               <$> genComputation tenv producert
               <*> pure var
               <*> genComputation (T.extend var vtArg tenv) ct]
  in sized $ \size -> oneof $ other size ++ case ct of
    T.Producer T.Unit -> onlyUnitProduce size ++ onlyProduce size
    T.Producer _      -> onlyProduce size
    T.Function _ _    -> onlyFunction size

shrinkValue :: (Eq var, Arbitrary var) => T.Value var var -> [T.Value var var]
shrinkValue (S.Thunk _ e) = [T.thunk e' | e' <- shrink e]
shrinkValue _ = []

shrinkToDuplication
  :: T.HCTy var var -> [T.Expression var var]
shrinkToDuplication t = [T.duplication | coerce t == T.Producer T.Unit]

shrinkExpression
  :: (Eq var, Arbitrary var) => T.Expression var var -> [T.Expression var var]
shrinkExpression (S.Duplication _) = []
shrinkExpression (S.Filter _ p) =
  [T.duplication] ++ [T.filter p' | p' <- shrink p]
shrinkExpression (S.Modification _ v1 v2) =
  [T.duplication] ++ [T.modification v1' v2' | (v1',v2') <- shrink (v1,v2)]
shrinkExpression (S.Iteration _ e) =
  [T.duplication]
  ++
  [e]
  ++
  [T.iteration_ e' | e' <- shrink e]
shrinkExpression (S.Produce t v) =
  shrinkToDuplication t
  ++
  [T.produce v' | v' <- shrink v]
shrinkExpression (S.Lambda t var e) = [S.Lambda t var e' | e' <- shrink e]
shrinkExpression (S.Sequential t e1 e2) =
  shrinkToDuplication t
  ++
  [e1 | T.typeOf e1 == coerce t]
  ++
  [e2]
  ++
  [T.sequential e1' e2' | (e1',e2') <- shrink (e1,e2)]
shrinkExpression (S.Parallel t e1 e2) =
  shrinkToDuplication t
  ++
  [e1,e2]
  ++
  [T.parallel e1' e2' | (e1',e2') <- shrink (e1,e2)]
shrinkExpression (S.Choice t e1 p e2) =
  shrinkToDuplication t
  ++
  [e1,e2]
  ++
  [T.choice e1' p e2' | (e1',e2') <- shrink (e1,e2)]
shrinkExpression (S.Application t e v) =
  shrinkToDuplication t
  ++
  [T.application_ e' v' | (e',v') <- shrink (e,v)]
shrinkExpression (S.Force t v) =
  shrinkToDuplication t
  ++
  [T.force_ v' | v' <- shrink v]
  ++
  [e | let S.Thunk _ e = v]
shrinkExpression (S.To t e1 var e2) =
  shrinkToDuplication t
  ++
  [e1 | coerce t == T.typeOf e1]
  ++
  [T.to e1' var e2' | (e1',e2') <- shrink (e1,e2)]

{-# LANGUAGE ScopedTypeVariables #-}

module ProbNetKat.Typing.ReconstructionSpec where

import Test.Hspec
import Test.QuickCheck (property, (==>))

import ProbNetKat.Result
import ProbNetKat.Typing.Arbitrary
import ProbNetKat.Syntax.Simple.Arbitrary


import qualified ProbNetKat.Typing as T
import qualified ProbNetKat.Typing.Reconstruction as R
import qualified ProbNetKat.Syntax.Simple as S
import           ProbNetKat.TypingSpec (successShouldSatisfy, welltyped)

import Text.Pretty

spec :: Spec
spec =
  describe "type reconstruction" $ do
    it "is complete and sound" $ property $
      -- Note by generating well-typed expressions and then reconstructing
      -- its type we verify completeness. By type-checking the reconstructed
      -- expressions we verify soundness as well.
      \(expr :: T.Expression Name Name) ->
        fmap Prettify (R.typeExpression (S.simplify S.absurdX expr))
        `successShouldSatisfy`
        (welltyped . uglify)

    it "is not unsound" $ property $
      \(expr :: S.Expression Name Name) ->
        let result = R.typeExpression expr
            Success r = result
        in isSuccess result ==> welltyped r `shouldBe` True
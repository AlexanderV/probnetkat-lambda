{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module ProbNetKat.Syntax.DeBruijn.Arbitrary where

import Test.QuickCheck (Arbitrary(arbitrary,shrink), shuffle)

import qualified ProbNetKat.Syntax.Simple.Arbitrary as SA
import qualified ProbNetKat.Syntax.DeBruijn as DB
import qualified ProbNetKat.Syntax.Simple as S
import           ProbNetKat.Syntax (freevars)

instance
  (Eq var, Arbitrary var) => Arbitrary (DB.Expression var var)
  where
    arbitrary = do
      simple <- arbitrary
      fv <- shuffle (freevars simple)
      return (DB.addDeBruijn fv simple)
    shrink debruijn =
      let fv = freevars debruijn
      in [DB.addDeBruijn fv e | e <- shrink (S.simplify S.absurdX debruijn)]

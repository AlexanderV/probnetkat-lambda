{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module ProbNetKat.Syntax.Simple.Arbitrary ( Name(..), Label(..) ) where

import Test.QuickCheck (Arbitrary(arbitrary,shrink),
                        sized,resize,oneof, choose, elements, suchThat,
                        NonNegative(getNonNegative))
import Test.QuickCheck.Gen (Gen)

import           ProbNetKat.Syntax.Simple as Simple
import qualified ProbNetKat.Syntax as S
import           Text.Pretty as P
import qualified Text.PrettyPrint.ANSI.Leijen as PP

-- | A newtype for identifiers
newtype Name = MkName { getName :: String } deriving (Eq,Ord)

instance Show Name where
  show = getName

instance Pretty Name where
  pretty = PP.string . getName

instance Arbitrary Name where
  arbitrary = elements [MkName [c] | c <- ['a'..'z']]

-- | A newtype for labels
newtype Label = MkLabel { getLabel :: String } deriving (Eq,Ord)

instance Show Label where
  show = getLabel

instance Pretty Label where
  pretty = PP.string . getLabel

instance Arbitrary Label where
  arbitrary = elements [MkLabel ('L':[c]) | c <- ['A'..'Z']]

-- | Arbitrary instance for simple values
instance (Arbitrary var, Arbitrary lvar) => Arbitrary (Value var lvar) where
  arbitrary = sized value where
    value 0 = oneof [genUnit, genVar, genLiteral, genLabel]
    value n = oneof [genUnit, genVar, genLiteral, genLabel, genThunk n]
    genUnit = return unit
    genVar = fmap variable arbitrary
    genLiteral = fmap (literal . getNonNegative) arbitrary
    genLabel = fmap (label . getLabel) arbitrary 
    genThunk n = fmap thunk (resize (n `div` 2) arbitrary)
  shrink (S.Thunk XU e) = map thunk (shrink e)
  shrink _ = []

instance (Arbitrary val) => Arbitrary (Predicate val)
  where
    arbitrary = sized predicate where
      predicate 0 = oneof [genDrop, genSkip]
      predicate n =
        oneof [genDrop, genSkip, genTest n, genDisj n, genConj n, genNeg n]
      genDrop = return Drop
      genSkip = return Skip
      genTest n = resize (n `div` 2) (Test <$> arbitrary <*> arbitrary)
      genDisj n = resize (n `div` 2) (Disjunction <$> arbitrary <*> arbitrary)
      genConj n = resize (n `div` 2) (Conjunction <$> arbitrary <*> arbitrary)
      genNeg n = resize (n - 1)      (Negation <$> arbitrary)
    shrink Drop = []
    shrink Skip = []
    shrink (Test v1 v2) =
      [Skip, Drop] ++[Test v1' v2' | (v1',v2') <- shrink (v1,v2)]
    shrink (Disjunction p1 p2) =
      [Skip,Drop] ++ [Disjunction p1' p2' | (p1',p2') <- shrink (p1,p2)]
    shrink (Conjunction p1 p2) =
      [Skip,Drop] ++ [Conjunction p1' p2' | (p1',p2') <- shrink (p1,p2)]
    shrink (Negation p) =
      [Skip,Drop] ++ [Negation p' | p' <- shrink p]

instance
  (Arbitrary var, Arbitrary lvar)
  => Arbitrary (Expression var lvar)
  where
    arbitrary = sized expr where
      expr 0 = oneof [genDuplication, genFilter]
      expr n = oneof [
        genDuplication,genFilter, genModification n,
        genParallel n, genSequential n, genChoice n, genIteration n,
        genLambda n, genForce n, genProduce n, genTo n, genApplication n]
      genDuplication = return duplication
      genFilter = fmap Simple.filter arbitrary
      genModification n =
        resize (n `div` 2) (modification <$> arbitrary <*> arbitrary)
      genParallel n =
        resize (n `div` 2) (parallel <$> arbitrary <*> arbitrary)
      genSequential n =
        resize (n `div` 2) (sequential <$> arbitrary <*> arbitrary)
      genChoice n = resize (n `div` 2) $
        choice <$> arbitrary <*> resize n arbitraryP <*> arbitrary
      arbitraryP = choose (0,1)
      genIteration n = resize (n - 1) (iteration <$> arbitrary)
      genLambda n = resize (n `div` 2) (lambda <$> arbitrary <*> arbitrary)
      genForce n = force <$> resize (n - 1) arbitrary
      genProduce n = produce <$> resize (n - 1) arbitrary
      genTo n =
        to <$> resize (n `div` 2) arbitrary
           <*> arbitrary
           <*> resize (n `div` 2) arbitrary
      genApplication n =
        application
        <$> resize (n `div` 2) arbitrary
        <*> resize (n `div` 2) arbitrary
    shrink (S.Duplication XU) = []
    shrink (S.Lambda XU lvar e) =
      [duplication] ++ [lambda lvar' e' | (lvar', e') <- shrink (lvar,e)]
    shrink (S.Filter XU p) =
      [duplication] ++ [Simple.filter p' | p' <- shrink p]
    shrink (S.Modification XU v1 v2) =
      [duplication] ++ [modification v1' v2' | (v1',v2') <- shrink (v1,v2)]
    shrink (S.Parallel XU e1 e2) =
      [duplication]
      ++ [e1,e2]
      ++ [parallel e1' e2' | (e1',e2') <- shrink (e1,e2)]
    shrink (S.Sequential XU e1 e2) =
      [duplication]
      ++ [e1,e2]
      ++ [sequential e1' e2' | (e1',e2') <- shrink (e1,e2)]
    shrink (S.Choice XU e1 p e2) =
      [duplication]
      ++ [e1,e2]
      ++ [choice e1' p e2' | (e1',e2') <- shrink (e1,e2)]
    shrink (S.Iteration XU e) =
      [duplication] ++ [iteration e' | e' <- shrink e]
    shrink (S.Force XU v) =
      [duplication] ++ [force v' | v' <- shrink v]
    shrink (S.Produce XU v) =
      [duplication] ++ [produce v' | v' <- shrink v]
    shrink (S.To XU e1 v e2) =
      [duplication]
      ++ [e1, e2]
      ++ [to e1' v' e2' | (e1',v',e2') <- shrink (e1,v,e2)]      
    shrink (S.Application XU e1 v) =
      [duplication]
      ++ [e1]
      ++ [application e1' v' | (e1',v') <- shrink (e1,v)]
module ProbNetKat.Syntax.UnparseSpec where

import Test.Hspec
import Test.QuickCheck (property)

import ProbNetKat.Syntax.Simple.Arbitrary

import qualified ProbNetKat.Syntax.Simple as S
import qualified ProbNetKat.Syntax.Parser as Parser
import qualified ProbNetKat.Syntax.Unparse as Unparse
import           Text.Trifecta.Parser (parseString,parseFromFileEx)
import           Text.Trifecta.Result (Result (Success))
import           Text.Parser.Combinators (eof)
import qualified Text.Pretty as P
import           Data.Bifunctor

parseV :: String -> Result (S.Value String String)
parseV str = parseString (Parser.value <* eof) mempty str

unparseV :: S.Value Name Name -> String
unparseV = show . P.pretty

parseE :: String -> Result (S.Expression String String)
parseE str = parseString (Parser.expression <* eof) mempty str

unparseE :: S.Expression Name Name -> String
unparseE = show . P.pretty

isSuccess :: Result a -> Bool
isSuccess (Success _) = True
isSuccess _ = False

getNames :: Bifunctor f => f Name Name -> f String String
getNames = bimap getName getName

spec :: Spec
spec =
  describe "Parsing" $ do
    it "is inverse to unparsing for values" $ property $
      \value -> parseV (unparseV value) `shouldSatisfy` isSuccess
    
    it "is inverse to unparsing for expressions" $ property $
      \expression -> parseE (unparseE expression) `shouldSatisfy` isSuccess

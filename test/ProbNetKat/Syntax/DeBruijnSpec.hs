{-# LANGUAGE ScopedTypeVariables #-}

module ProbNetKat.Syntax.DeBruijnSpec where

import Test.Hspec
import Test.QuickCheck (property, NonNegative(..), (==>))

import ProbNetKat.Syntax.Simple.Arbitrary
import ProbNetKat.Syntax.DeBruijn.Arbitrary
import ProbNetKat.Syntax (
  Expression(Force,Lambda,To),Value(Variable),
  freevars)

import qualified ProbNetKat.Syntax.DeBruijn as DB

name :: Name
name = MkName "a"

env1, env2 :: Int -> Maybe (DB.Value String String)
env1 0 = Just (DB.variable 0 "right")
env1 _ = Nothing
env2 0 = Just (DB.label "WRONG")
env2 _ = Nothing


spec :: Spec
spec = do
  describe "shifting" $ do
    it "is idempotent with inverse distances" $ property $
      \(deBruijn :: DB.Expression Name Name) (NonNegative distance) ->
        let doubleShift = DB.shiftE 0 (-distance) . DB.shiftE 0 distance
        in doubleShift deBruijn `shouldBe` deBruijn

    it "shifts variables by the right distance" $ property $
      \(NonNegative n) d ->
        DB.shiftV 0 d (DB.variable n name)
        `shouldBe`
        (DB.variable (n + d) name :: DB.Value Name Name)

    it "ignores variables below the cutoff" $ property $ \n name distance ->
      let v = DB.variable n name :: DB.Value Name Name
      in DB.shiftV (n + 1) distance v `shouldBe` v

    it "increases the cutoff accross lambdas" $
      (DB.shiftE 0 1 $ DB.lambda name $ DB.produce $ DB.variable 0 name)
      `shouldBe`
      (DB.lambda name $ DB.produce $ DB.variable 0 name)
      
    it "increases the cutoff accross to" $
      (DB.shiftE 0 1 $ DB.to DB.duplication name $ DB.produce $ DB.variable 0 name)
      `shouldBe`
      (DB.to DB.duplication name $ DB.produce $ DB.variable 0 name)
      
  describe "substitution" $ do
    it "with a non-free variable has no effect" $ property $
      \(deBruijn :: DB.Expression Name Name) ->
        let fv = freevars deBruijn :: [Name]
            env :: Int -> Maybe (DB.Value Name Name)
            env n | n < length fv = Nothing
                  | otherwise = Just (DB.label "WRONG")
        in DB.substitute env deBruijn `shouldBe` deBruijn

    it "avoids variable capture in lambdas" $
      case DB.substitute env1 (DB.lambda "" $ DB.force $ DB.variable 1 "") of
        Lambda _ _ e1 ->
          DB.substitute env2 e1 `shouldSatisfy` (\e2 -> case e2 of
            Force _ (Variable _ "right") -> True
            _ -> False)

    it "avoids variable capture in to" $
      case DB.substitute env1 (DB.to DB.duplication "" $ DB.force $ DB.variable 1 "") of
        To _ _ _ e1 ->
          DB.substitute env2 e1 `shouldSatisfy` (\e2 -> case e2 of
            Force _ (Variable _ "right") -> True
            _ -> False)
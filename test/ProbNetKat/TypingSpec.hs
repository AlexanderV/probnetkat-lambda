{-# LANGUAGE ScopedTypeVariables #-}

module ProbNetKat.TypingSpec where

import Test.Hspec
import Test.QuickCheck (property, (==>))
import Data.Coerce

import ProbNetKat.Typing.Arbitrary
import ProbNetKat.Syntax.Simple.Arbitrary

import           ProbNetKat.Result
import qualified ProbNetKat.Typing as T
import qualified ProbNetKat.Syntax.Simple as S
import qualified ProbNetKat.Syntax as Syntax

import Text.Pretty


spec :: Spec
spec =
  describe "type checking" $ do
    it "succeeds on well-typed closed values" $ property $
      \(value :: T.Value Name Name) ->
        T.checkV mempty value `successShouldBe` T.typeOfV value
        
    it "succeeds on well-typed closed expressions" $ property $
      \(expr :: T.Expression Name Name) ->
        T.check mempty expr `successShouldBe` T.typeOf expr
        
    it "succeeds on well-typed expressions" $ property $
      \(MkExprConf env expr :: ExprConf Name) ->
        T.check env expr `successShouldBe` T.typeOf expr
        
    it "rejects ill-scoped expressions" $ property $
      \(MkExprConf _ expr :: ExprConf Name) ->
        not (null (Syntax.freevars expr :: [Name]))
        ==> T.check mempty expr `shouldSatisfy` isFailure
        
    it "rejects ill-typed unit values" $ property $
      let var :: T.Value String String
          var = Syntax.Unit (coerce T.Field)
      in T.checkV mempty var `shouldSatisfy` isFailure

successShouldBe :: (Show a, Eq a) => Result a -> a -> Expectation
successShouldBe (Success y) x = y `shouldBe` x
successShouldBe (Failure errMsg) _ =
  expectationFailure ("result unsuccesful with failure: " ++ show errMsg)

successShouldSatisfy
  :: (Show a, Eq a) => Result a -> (a -> Bool) -> Expectation
successShouldSatisfy (Success y) predicate = y `shouldSatisfy` predicate
successShouldSatisfy (Failure errMsg) _ =
  expectationFailure ("result unsuccesful with failure: " ++ show errMsg)

welltyped :: (Pretty var, Eq var) => T.Expression var var -> Bool
welltyped = isSuccess . T.check mempty

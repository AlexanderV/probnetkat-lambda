module lambda-probabilistic-netkat.

value unit.
value (thunk E).
value field.
value literal.

terminal (produce V).
terminal (abs T F).
terminal (pred P).
terminal (mod _ _).
terminal dup.
terminal (seq T1 T2) :- terminal T1, terminal T2.
terminal (par T1 T2) :- terminal T1, terminal T2.
terminal (choice T1 T2) :- terminal T1, terminal T2.
terminal (iter T) :- terminal T.

probabilistic (pred P).
probabilistic (mod _ _).
probabilistic dup.
probabilistic (par A B) :- probabilistic A, probabilistic B.
probabilistic (seq A B) :- probabilistic A, probabilistic B.
probabilistic (choice A B) :- probabilistic A, probabilistic B.
probabilistic (iter A) :- probabilistic A.

type_of_c (pred skip) (producer one).
type_of_c (pred drop) (producer one).
type_of_c (pred (guard F V)) (producer one) :-
  type_of_v F fld, type_of_v V lit.
type_of_c (pred (conj P1 P2)) (producer one) :-
  type_of_c (pred P1) (producer one), type_of_c (pred P2) (producer one).
type_of_c (pred (disj P1 P2)) (producer one) :-
  type_of_c (pred P1) (producer one), type_of_c (pred P2) (producer one).
type_of_c (pred (neg P)) (producer one) :- type_of_c (pred P) (producer one).

type_of_c (mod F V) (producer one) :-
  type_of_v F fld, type_of_v V lit.
type_of_c dup (producer one).

type_of_c (seq A B) T :- type_of_c A T', type_of_c B T.
type_of_c (par A B) (producer one) :- 
  type_of_c A (producer one), type_of_c B (producer one).
type_of_c (choice A B) T :- type_of_c A T, type_of_c B T.
type_of_c (iter C) (producer one) :- type_of_c C (producer one).

type_of_c (produce A) (producer T) :-
  type_of_v A T.
type_of_c (force V) T :- type_of_v V (thnk T).
type_of_c (app A V) T2 :-
  type_of_c A (fun T1 T2), type_of_v V T1.
type_of_c (abs T1 F) (fun T1 T2) :-
  pi x\ type_of_v x T1 => type_of_c (F x) T2.
type_of_c (to A F) T2 :-
  type_of_c A (producer T1), pi x\ type_of_v x T1 => type_of_c (F x) T2.

type_of_v unit one.
type_of_v (thunk A) (thnk T) :- type_of_c A T.
type_of_v field fld.
type_of_v literal lit.

% big step evaluation relation
% Call-By-Push-Value
step (pred P) (pred P).
step (mod F V) (mod F V).
step dup dup.
step (produce V) (produce V).
step (abs Ty F) (abs Ty F).

step (seq A B) (seq A' B') :- step A A', step B B'.
step (par A B) (par A' B') :- step A A', step B B'.
step (choice A B) (choice A' B') :- step A A', step B B'.
step (iter A) (iter A') :- step A A'.
step (force (thunk A)) A' :- step A A'.

step (app A V) A' :- step A (abs T F), step (F V) A'.
step (app A V) (seq B C') :-
  step A (seq B C), step (app C V) C'.
step (app A V) (choice B' C') :-
  step A (choice B C), step (app B V) B', step (app C V) C'.
  
step (to A F) (seq (pred P) B) :- step A (pred P), step (F unit) B.
step (to A F) (seq (mod Fld V) B) :- step A (mod Fld V), step (F unit) B.
step (to A F) (seq dup B) :- step A dup, step (F unit) B.
step (to A F) B :- step A (produce V), step (F V) B.
step (to A F)  (seq B D) :-
  step A (seq B C), step (to C F) D.
step (to A F)  (seq (par B C) D) :-
  step A (par B C), step (F unit) D.
step (to A F)  (choice B' C') :-
  step A (choice B C), step (to B F) B', step (to C F) C'.
step (to A F) (seq (iter A') B') :-
  step A (iter A'), step (F unit) B'.

nf_atp (pred skip) (producer one).
nf_atp (pred drop) (producer one).
nf_atp (pred (guard F V)) (producer one) :-
  nf_nv F fld, nf_nv V lit.
nf_atp (pred (conj P1 P2)) (producer one) :-
  nf_atp (pred P1) (producer one), nf_atp (pred P2) (producer one).
nf_atp (pred (disj P1 P2)) (producer one) :-
  nf_atp (pred P1) (producer one), nf_atp (pred P2) (producer one).
nf_atp (pred (neg P)) (producer one) :-
  nf_atp (pred P) (producer one).
nf_atp (mod F V) (producer one) :-
  nf_nv F fld, nf_nv V lit.
nf_atp dup (producer one).
nf_atp (iter C) (producer one) :- nf_nc C (producer one).

nf_at (force V) T :- nf_at_v V (thnk T).
nf_at (app C V) T2 :- nf_at C (fun T1 T2), nf_nv V T1.
nf_at (to C F) T2 :-
  nf_at C (producer T1), pi x\ nf_at_v x T1 => nf_nc (F x) T2.

nf_nv unit one.
nf_nv (thunk C) (thnk T) :- nf_nc C T.
nf_nv field fld.
nf_nv literal lit.
nf_nv V T :- nf_at_v V T.

nf_seq C T :- nf_at C T.
nf_seq C T :- nf_atp C T.
nf_seq (seq C1 C2) T :- nf_seq C1 T', nf_at C2 T.
nf_seq (seq C1 C2) T :- nf_seq C1 T', nf_atp C2 T.

nf_abs C T :- nf_seq C T.
nf_abs (abs T1 F) (fun T1 T2) :- pi x\ nf_at_v x T1 => nf_nc (F x) T2.
nf_abs (seq C (abs T1 F)) (fun T1 T2) :-
  nf_seq C T', pi x\ nf_at_v x T1 => nf_nc (F x) T2.
nf_abs (produce V) (producer T) :- nf_nv V T.
nf_abs (seq C (produce V)) (producer T) :-
  nf_seq C T', nf_nv V T.

nf_par C T :- nf_abs C T.
nf_par (par C1 C2) T :- nf_par C1 T, nf_abs C2 T.

nf_nc C T :- nf_par C T.
nf_nc (choice C1 C2) T :- nf_nc C1 T, nf_par C2 T.

erase (pred P) (pred P).
erase (mod F V) (mod F V).
erase dup dup.
erase (seq C1 C2) (seq C3 C4) :- erase C1 C3, erase C2 C4.
erase (par C1 C2) (par C3 C4) :- erase C1 C3, erase C2 C4.
erase (choice C1 C2) (choice C3 C4) :- erase C1 C3, erase C2 C4.
erase (iter C1) (iter C2) :- erase C1 C2.
erase (produce V) (pred skip).
erase (abs S F) (pred skip).

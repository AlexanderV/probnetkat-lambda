Specification "lambda-probabilistic-netkat".

Import "ctx_properties".

Theorem unique_types:
  (forall L T1 T2 A, ctx L -> {L |- type_of_c A T1} -> {L |- type_of_c A T2} -> T1 = T2)
  /\
  (forall L T1 T2 V, ctx L -> {L |- type_of_v V T1} -> {L |- type_of_v V T2} -> T1 = T2).
induction on 2 2. split.
  % 1. Uniqueness of types for computations.
  intros.
  case H3.
    % 1.1 T-Pred skip 
    case H2. search.
    apply ctx_member to H1 H5. case H4.
    % 1.2 T-Pred drop
    case H2. search.
    apply ctx_member to H1 H5. case H4.
    % 1.3 T-Pred guard
    case H2. search.
    apply ctx_member to H1 H7. case H6.
    % 1.4 T-Pred conj
    case H2. search.
    apply ctx_member to H1 H7. case H6. 
    % 1.5 T-Pred disj
    case H2. search.
    apply ctx_member to H1 H7. case H6.
    % 1.6 T-Pred Not
    case H2. search.
    apply ctx_member to H1 H6. case H5.
    % 1.7 T-Mod
    case H2. search.
    apply ctx_member to H1 H7. case H6.
    % 1.8 T-Dup
    case H2. search.
    apply ctx_member to H1 H5. case H4.
    % 1.9 T-Seq
    case H2. apply IH to H1 H7 H5. search.
    apply ctx_member to H1 H7. case H6.
    % 1.10 T-Par
    case H2. apply IH to H1 H7 H5. apply IH to H1 H6 H4. search.
    apply ctx_member to H1 H7. case H6. 
    % 1.11 T-Choice
    case H2. apply IH to H1 H7 H5. apply IH to H1 H6 H4. search.
    apply ctx_member to H1 H7. case H6. 
    % 1.12 T-Iter
    case H2. search.
    apply ctx_member to H1 H6. case H5.
    % 1.13 T-Produce
    case H2. apply IH1 to H1 H5 H4. search.
    apply ctx_member to H1 H6. case H5.
    % 1.14 T-Force
    case H2. apply IH1 to H1 H5 H4. search.
    apply ctx_member to H1 H6. case H5.
    % 1.15 T-App
    case H2. apply IH to H1 H6 H4. search.
    apply ctx_member to H1 H7. case H6.
    % 1.16 T-Abs
    case H2. apply IH to _ H5 H4. search.
    apply ctx_member to H1 H6. case H5.
    % 1.17 T-To
    case H2. apply IH to H1 H6 H4. apply IH to _ H7 H5. search.
    apply ctx_member to H1 H7. case H6.
    % 1.17 T-Var
    apply ctx_member to H1 H5. case H4.
  % 2. Uniqueness of types for values
  intros.
  case H3.
    % 2.1 T-Unit
    case H2. search.
    apply ctx_member to H1 H5. case H4. case H6.
    % 2.2 T-Thunk
    case H2. apply IH to H1 H5 H4. search.
    apply ctx_member to H1 H6. case H5. case H7.
    % 2.3 T-Field
    case H2. search.
    apply ctx_member to H1 H5. case H4. case H6.
    % 2.4 T-Lit
    case H2. search.
    apply ctx_member to H1 H5. case H4. case H6.
    % 2.5 T-Var
    apply ctx_member to H1 H5.
    case H2.
      % 2.5.1
      case H4. case H6. % absurd
      % 2.5.2
      case H4. case H6. % absurd
      % 2.5.3
      case H4. case H6. % absurd
      % 2.5.4
      case H4. case H6. % absurd
      % 2.5.3 based on uniqueness of names in typing contexts.
      case H4. case H6.
      apply ctx_member to H1 H8.
      case H7.
      apply ctx_uniq to H1 H5 H8.
      search.

Theorem unique_value_types:
  forall L T1 T2 V,
  ctx L -> {L |- type_of_v V T1} -> {L |- type_of_v V T2} -> T1 = T2.
apply unique_types. search.

Theorem unique_computation_types:
  forall L T1 T2 A,
  ctx L -> {L |- type_of_c A T1} -> {L |- type_of_c A T2} -> T1 = T2.
apply unique_types. search.
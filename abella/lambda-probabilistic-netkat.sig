sig lambda-probabilistic-netkat.

kind pr, tm, ty_v, ty_c, ty_r type.

type one ty_v.
type thnk ty_c -> ty_v.
type fld ty_v.
type lit ty_v.

type fun ty_v -> ty_c -> ty_c.
type producer ty_v -> ty_c.

type ty_r_v ty_v -> ty_r.
type ty_r_t ty_c -> ty_r.
type ty_r_c ty_c -> ty_r.

type unit tm.
type thunk tm -> tm.
type field tm.
type literal tm.

type skip pr.
type drop pr.
type guard tm -> tm -> pr.
type conj pr -> pr -> pr.
type disj pr -> pr -> pr.
type neg pr -> pr.

type pred pr -> tm.
type mod tm -> tm -> tm.
type dup tm.

type par tm -> tm -> tm.
type seq tm -> tm -> tm.
% note seq can be desugared to seg A B = to A (x\B) if we remove the type from
% to. This does break the reflection on probabilistic values. 
type choice tm -> tm -> tm.
type iter tm -> tm.

type produce tm -> tm.
type force tm -> tm.
type to tm -> (tm -> tm) -> tm.
type app tm -> tm -> tm.
type abs ty_v -> (tm -> tm) -> tm.

type value tm -> o.
type terminal tm -> o.
type probabilistic tm -> o.
type type_of_v tm -> ty_v -> o.
type type_of_c tm -> ty_c -> o.
type step tm -> tm -> o.

type nf_atp tm -> ty_c -> o.
type nf_at_v tm -> ty_v -> o.
type nf_at tm -> ty_c -> o.
type nf_nv tm -> ty_v -> o.
type nf_seq tm -> ty_c -> o.
type nf_abs tm -> ty_c -> o.
type nf_par tm -> ty_c -> o.
type nf_nc tm -> ty_c -> o.

type erase tm -> tm -> o.

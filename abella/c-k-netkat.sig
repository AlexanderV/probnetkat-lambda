sig c-k-netkat.

accum_sig lambda-probabilistic-netkat.

kind stack type.

type empty stack.
type sval tm -> stack -> stack.
type sto (tm -> tm) -> stack -> stack.
type lpar tm -> stack -> stack. % par [] B
type rpar tm -> stack -> stack. % par A []
type lchoice tm -> stack -> stack. % choice [] B
type rchoice tm -> stack -> stack. % choice A []
type sseq tm -> stack -> stack.
type siter stack -> stack.

type type_of_s ty_c -> stack -> ty_c -> o.
type transition tm -> stack -> tm -> stack -> o.
type transition* tm -> stack -> tm -> stack -> o.
type terminal_conf tm -> stack -> o.
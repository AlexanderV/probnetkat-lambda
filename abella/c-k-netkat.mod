module c-k-netkat.

accumulate lambda-probabilistic-netkat.

% outside typing.
type_of_s T empty T.
type_of_s T (sval V S) (fun T1 T2) :- type_of_v V T1, type_of_s T S T2.
type_of_s T (sto F S) (producer TV) :-
  (pi x\ type_of_v x TV => type_of_c (F x) T2), type_of_s T S T2.
type_of_s T (lpar B S) T1 :-
  type_of_c B T1, type_of_s T S T1.
type_of_s T (rpar A S) T1 :-
  terminal A, type_of_c A T1, type_of_s T S T1.
type_of_s T (lchoice B S) T1 :-
  type_of_c B T1, type_of_s T S T1.
type_of_s T (rchoice A S) T1 :-
  terminal A, type_of_c A T1, type_of_s T S T1.
type_of_s T (sseq A S) _ :-
  type_of_c A T1, type_of_s T S T1.
type_of_s T (siter S) (producer one) :-
  type_of_s T S (producer one).

% transition pred S (produce unit) S.
% transition mod S (produce unit) S.
% transition (par A B) S A (lpar B S).
% transition (seq A B) S A (sseq B S).
% transition (choice A B) S A (lchoice B S).
% transition (iter A) S A (siter S).
% transition (app A V) S A (sval V S).
% transition (abs T F) (sval V S) (F V) S.
% transition (force (thunk A)) S A S.
% transition (to A F) S A (sto F S).
% transition T (sseq B S) B S :- terminal T.
% transition T (lpar B S) B (rpar T S) :- terminal T.
% transition TB (rpar TA S) T S :- terminal TB, combine par TA TB T.
% transition T (lchoice B S) B (rchoice T S) :- terminal T.
% transition TB (rchoice TA S) T S :- terminal TB, combine choice TA TB T.
% transition (produce V) (sto F S) (F V) S.
% transition (produce unit) (siter S) (produce unit) S.

transition* A S A S.
transition* A S B R :- transition A S C Q, transition* C Q B R.

terminal_conf (produce V) empty.
terminal_conf (abs _ A)   empty.
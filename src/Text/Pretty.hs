{- | 

Module:      Text.Pretty
Description: Pretty-printer class and pretty printing utilities.
Maintainer:  alexander.vandenbroucke@kuleuven.be

Pretty-printer class and pretty printing utilities: the type class 'Pretty'
defines a 'prettyPrec' function, similar to 'showsPrec' from 'Show'.
This function is helpful when pretty printing values that need nested
parentheses, such as program syntax or ASTs.

-}

{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Text.Pretty (
  Pretty(prettyPrec, pretty),
  Prettify(Prettify, uglify),
  parens,
  ampersand,
  putPretty,
  putPrettyLn,
  putDoc,
  putDocLn,
  hPutDoc,
  hPutDocLn
)
where

import qualified Text.PrettyPrint.ANSI.Leijen as PP
import           Text.PrettyPrint.ANSI.Leijen (Doc, (<>), (</>))
import           System.IO (stdout,Handle)

-- | A class for pretty printing
class Pretty a where
  -- | Pretty print within a precedence context, see 'showsPrec'.
  prettyPrec :: Int -> a -> Doc
  prettyPrec _ = pretty

  -- | Pretty print a value
  pretty :: a -> Doc
  pretty = prettyPrec 11
  {-# MINIMAL prettyPrec | pretty #-}

instance Pretty () where
  prettyPrec _ () = PP.string "()"

instance (Pretty a, Pretty b) => Pretty (a,b) where
  prettyPrec _ (x,y) = PP.parens (pretty x <> PP.comma <> pretty y)

instance (Pretty a, Pretty b, Pretty c) => Pretty (a,b,c) where
  prettyPrec _ (x,y,z) =
    PP.parens (pretty x <> PP.comma </> pretty y <> PP.comma </> pretty z)

instance Pretty Int where
  prettyPrec _ = PP.int

instance Pretty Integer where
  prettyPrec _ = PP.integer
    
instance Pretty Double where
  prettyPrec _ = PP.double

instance {-# OVERLAPPING #-} Pretty String where
  prettyPrec _ = PP.string

instance Pretty a => Pretty (Maybe a) where
  prettyPrec _ Nothing = PP.string "Nothing"
  prettyPrec p (Just a) = parens (p < 1) (PP.string "Just " <> prettyPrec 0 a)

instance {-# OVERLAPPABLE #-} Pretty a => Pretty [a] where
  prettyPrec _ = PP.list . map pretty

-- | Newtype that replaces the 'Show' instance with the 'Pretty' instance.
newtype Prettify a = Prettify { uglify :: a } deriving (Eq,Ord)

instance Pretty Doc where
  prettyPrec _ = id

instance Pretty a => Show (Prettify a) where
  show (Prettify x) = show (pretty x)


-- | Wrap a string in parentheses.
parens :: Bool -> Doc -> Doc
parens True  doc = PP.parens doc
parens False doc = doc

-- | A 'Doc' containing the ampersand (&)
ampersand :: Doc
ampersand = PP.char '&'

-- | Write an instance of 'Pretty'.
-- Output with a page width of 80 characters and a ribbon width of 68.
putPretty :: Pretty a => a -> IO ()
putPretty = putDoc . pretty


-- | Write an instance of 'Pretty', like 'putPretty' but adds a newline.
putPrettyLn :: Pretty a => a -> IO ()
putPrettyLn =
  PP.displayIO stdout . PP.renderSmart 0.85 80 . (<> PP.hardline) . pretty


-- | Write a 'Doc' with a page width of 80 characters and a ribbon width of 68.
putDoc :: Doc -> IO ()
putDoc = hPutDoc stdout

-- | Like 'putDoc' but adds a newline.
putDocLn :: Doc -> IO ()
putDocLn doc = putDoc (doc <> PP.hardline) 

-- | Write a 'Doc' with a page widht of 80 characters and a ribbon width of 68
-- to a file handle.
hPutDoc :: Handle -> Doc -> IO ()
hPutDoc handle = PP.displayIO handle . PP.renderSmart 0.85 80

-- | Like 'hPutDoc' but adds a newline.
hPutDocLn :: Handle -> Doc -> IO ()
hPutDocLn handle doc = hPutDoc handle (doc <> PP.hardline)

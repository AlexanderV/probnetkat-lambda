{- |

Module:      ProbNetKat.Syntax
Description: Syntax tree datatypes for ProbNetKAT.
Maintainer:  alexander.vandenbroucke@kuleuven.be

This module contains Syntax tree data types for ProbNetKAT.

There are separate datatypes for predicates and expressions, as the paper
explains, this is to prevent negation of programs (e.g. nonsensical things
like ~(src <- 2)).

-}

{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveGeneric #-}

module ProbNetKat.Syntax (
  -- * Type Synonyms
  Field,
  Probability,
  -- * Predicates
  Value(..),
  Predicate(..),
  -- * Expressions
  -- $trees-that-grow
  Expression(..),
  XUnit,XThunk,XLabel,XLiteral,XVariable,
  XDuplication, XFilter, XModification, XParallel, XSequential, XChoice,
  XIteration, XLambda, XForce, XProduce, XTo, XApplication, XExpression,
  XAll0,
  XAll2,
  Program(..),
  xifthenelse,
  xcollapsed,
  xfoldV,
  xfold,
  Substitute(substitute),
  FreeVars(freevars),
  prettyProgram
)
where

import           Control.DeepSeq
import           Data.Bifunctor
import           Data.List (union,(\\))
import           Data.Maybe (fromMaybe)
import           GHC.Exts (Constraint)
import           GHC.Generics
import           Text.Pretty
import           Text.PrettyPrint.ANSI.Leijen ((<+>))
import qualified Text.PrettyPrint.ANSI.Leijen as PP


-- | Identfier of a packet field
type Field = String

-- | Probability type
type Probability = Double

-- | Syntax tree for predicates
data Predicate val
  = Drop
  | Skip
  | Test !val !val
  | Disjunction !(Predicate val) !(Predicate val)
  | Conjunction !(Predicate val) !(Predicate val)
  | Negation !(Predicate val)
  deriving (Show, Eq, Functor, Foldable, Traversable,Generic)

instance NFData val => NFData (Predicate val)

{- $trees-that-grow
The 'Expression' data type uses the
<https://www.microsoft.com/en-us/research/uploads/prod/2016/11/trees-that-grow.pdf Trees that grow>
approach to make the syntax type extensible.

The idea is that every constructor has an additional field of a type that is
described by a type family. The naming convention for these type families
is XConstructor. For instance for 'Duplication' this is 'XDuplication'.

Specific extensions use these fields to annotate the constructor with specific
values. For instance, typed syntax annotates each term with its type, or
the pretty printing extension annotates each term with its pretty printed
version.
For more concrete examples see
"ProbNetKat.Syntax.Unparse" or "ProbNetKat.Syntax.Simple".

Finally, 'Expression' has the @Expression@ constructor that allows extending
the type with arbitrary additional constructors through the 'XExpression' type
family.
-}

-- | Extension for 'Unit'
type family XUnit ext :: * -> * -> *
-- | Extension for 'Thunk'
type family XThunk ext :: * -> * -> *  
-- | Extension for 'Literal'
type family XLiteral ext :: * -> * -> *
-- | Extension for 'Label'
type family XLabel ext :: * -> * -> *

-- | Extension for 'Duplication' 
type family XDuplication ext :: * -> * -> *
-- | Extension for 'Filter'
type family XFilter ext :: * -> * -> *
-- | Extension for 'Modification'
type family XModification ext :: * -> * -> *
-- | Extension for 'Parallel'
type family XParallel ext :: * -> * -> *
-- | Extension for 'Sequential'
type family XSequential ext :: * -> * -> *
-- | Extension for 'Choice' 
type family XChoice ext :: * -> * -> *
-- | Extension for 'Iteration'
type family XIteration ext :: * -> * -> *
-- | Extension for 'Variable'
type family XVariable ext :: * -> * -> *
-- | Extension for 'Lambda'
type family XLambda ext :: * -> * -> *
-- | Extension for 'Application'
type family XApplication ext :: * -> * -> *
-- | Extension for 'Force'
type family XForce ext :: * -> * -> *
-- | Extension for 'Produce'
type family XProduce ext :: * -> * -> *
-- | Extension for 'To'
type family XTo ext :: * -> * -> *
-- | Extension for 'Expression'
type family XExpression ext :: * -> * -> *

-- | Syntax tree for values.
-- Uses
-- <https://www.microsoft.com/en-us/research/uploads/prod/2016/11/trees-that-grow.pdf Trees that grow>.
--
-- But the actual structure is described by the following grammar, which is
-- quite simple.
-- > V = Unit
-- >   | Thunk E
-- >   | Literal Integer
-- >   | Label String
-- >   | Variable var  
data Value ext var lvar
 = Unit !(XUnit ext var lvar)
 | Thunk !(XThunk ext var lvar) !(Expression ext var lvar)
 | Label !(XLabel ext var lvar) !String
 | Literal !(XLiteral ext var lvar) !Integer
 | Variable !(XVariable ext var lvar) !var
 deriving Generic

deriving instance
  (XAll0 Show ext var lvar, Show var, Show lvar)
  => Show (Value ext var lvar)

deriving instance
  (XAll0 Eq ext var lvar, Eq var, Eq lvar)
  => Eq (Value ext var lvar)

instance
  (XAll0 Generic ext var lvar, XAll0 NFData ext var lvar,
   Generic var, Generic lvar,
    NFData var, NFData lvar) =>
  NFData (Value ext var lvar)

instance XAll2 Bifunctor ext => Bifunctor (Value ext) where
  bimap f g (Unit x)  = Unit (bimap f g x)
  bimap f g (Thunk x e) = Thunk (bimap f g x) (bimap f g e)
  bimap f g (Label x lab) = Label (bimap f g x) lab
  bimap f g (Literal x lit) = Literal (bimap f g x) lit
  bimap f g (Variable x var) = Variable (bimap f g x) (f var)

-- | Syntax tree for expressions.
-- Uses
-- <https://www.microsoft.com/en-us/research/uploads/prod/2016/11/trees-that-grow.pdf Trees that grow>.
--
-- But the actual structure is described by the following grammar, which is
-- quite simple.
--
-- > E = Duplication
-- >   | Filter Predicate
-- >   | Modification Field Int
-- >   | Parallel E E
-- >   | Sequential E E
-- >   | Choice E Double E
-- >   | Iteration E  
-- >   | Variable var
-- >   | Lambda lvar E
-- >   | Application E E
--
data Expression ext var lvar
  = Duplication  !(XDuplication ext var lvar)
  | Filter       !(XFilter ext var lvar)
      !(Predicate (Value ext var lvar))
  | Modification !(XModification ext var lvar)
      !(Value ext var lvar) !(Value ext var lvar)
  | Parallel     !((XParallel ext) var lvar)
      !(Expression ext var lvar) (Expression ext var lvar)
  | Sequential   !((XSequential ext) var lvar)
      !(Expression ext var lvar) !(Expression ext var lvar)
  | Choice       !(XChoice ext var lvar)
      !(Expression ext var lvar) !Probability !(Expression ext var lvar)
  | Iteration    !(XIteration ext var lvar)
      !(Expression ext var lvar)
  | Lambda       !(XLambda ext var lvar)
      !lvar !(Expression ext var lvar)
  | Application  !(XApplication ext var lvar)
      !(Expression ext var lvar) !(Value ext var lvar)
  | Force !(XForce ext var lvar)
      !(Value ext var lvar)
  | Produce !(XProduce ext var lvar)
      !(Value ext var lvar)
  | To !(XTo ext var lvar)
      !(Expression ext var lvar) !lvar !(Expression ext var lvar)
  | Expression   !(XExpression ext var lvar)
  deriving Generic

-- | Type family for constraints expecting kind @*@, over all extensions.
type family XAll0 (c :: * -> Constraint) ext var lvar :: Constraint
type instance XAll0 c ext var lvar = (
  c (XUnit ext var lvar),
  c (XThunk ext var lvar),
  c (XLabel ext var lvar),
  c (XLiteral ext var lvar),
  c (XDuplication ext var lvar),
  c (XFilter ext var lvar),
  c (XModification ext var lvar),
  c (XParallel ext var lvar),
  c (XSequential ext var lvar),
  c (XChoice ext var lvar),
  c (XIteration ext var lvar),
  c (XVariable ext var lvar),
  c (XLambda ext var lvar),
  c (XApplication ext var lvar),
  c (XForce ext var lvar),
  c (XProduce ext var lvar),
  c (XTo ext var lvar),
  c (XExpression ext var lvar))
  
-- | Type family for constraints expecting kind @* -> * -> *@, for all extensions.
type family XAll2 (c :: (* -> * -> *) -> Constraint) ext :: Constraint
type instance XAll2 c ext = (
  c (XUnit ext),
  c (XThunk ext),
  c (XLabel ext),
  c (XLiteral ext),
  c (XDuplication ext),
  c (XFilter ext),
  c (XModification ext),
  c (XParallel ext),
  c (XSequential ext),
  c (XChoice ext),
  c (XIteration ext),
  c (XVariable ext),
  c (XLambda ext),
  c (XApplication ext),
  c (XForce ext),
  c (XProduce ext),
  c (XTo ext),
  c (XExpression ext))

deriving instance
  (XAll0 Show ext var lvar, Show var, Show lvar)
  => Show (Expression ext var lvar)

deriving instance
  (XAll0 Eq ext var lvar, Eq var, Eq lvar)
  => Eq (Expression ext var lvar)

instance
  (XAll0 Generic ext var lvar, XAll0 NFData ext var lvar,
   Generic var, Generic lvar,
    NFData var, NFData lvar) =>
  NFData (Expression ext var lvar)

instance XAll2 Bifunctor ext => Bifunctor (Expression ext) where
  bimap f g (Duplication x)  = Duplication (bimap f g x)
  bimap f g (Filter x p) = Filter (bimap f g x) (fmap (bimap f g) p)
  bimap f g (Modification x field val) =
    Modification (bimap f g x) (bimap f g field) (bimap f g val)
  bimap f g (Parallel x e1 e2)    =
    Parallel (bimap f g x) (bimap f g e1) (bimap f g e2)
  bimap f g (Sequential x e1 e2)  =
    Sequential (bimap f g x) (bimap f g e1) (bimap f g e2)
  bimap f g (Choice x e1 p e2)    =
    Choice (bimap f g x) (bimap f g e1) p (bimap f g e2)
  bimap f g (Iteration x e) = Iteration (bimap f g x) (bimap f g e)
  bimap f g (Lambda x var e) = Lambda (bimap f g x) (g var) (bimap f g e)
  bimap f g (Application x e1 e2) =
    Application (bimap f g x) (bimap f g e1) (bimap f g e2)
  bimap f g (Force x v) = Force (bimap f g x) (bimap f g v)
  bimap f g (Produce x v) = Produce (bimap f g x) (bimap f g v)
  bimap f g (To x e1 var e2) =
    To (bimap f g x) (bimap f g e1) (g var) (bimap f g e2)
  bimap f g (Expression x) = Expression (bimap f g x)

-- | Datatype for a program.
--
--   A Program consists of several top-level assignments of expressions to
--   variables, and a single main expression.
data Program ext var lvar =
  MkProgram
  {
    _toplevels       :: [(lvar, Expression ext var lvar)],
    _main_expression :: Expression ext var lvar
  }
  
deriving instance
  (Show var, Show lvar, XAll0 Show ext var lvar)
  => Show (Program ext var lvar)

-- | A class of types @t@ where @value@s can be substituted for @var@.
class Substitute var value t where
  -- | Substitute variables in @t@.
  substitute :: (var -> Maybe value) -> t -> t

-- | A class of types @t@ which contain free variables @var@.
class FreeVars var t where
  -- | Collect the free variables in a 'Predicate'
  freevars :: t -> [var]
  

-- | Substitute variables inside a predicate.
instance Substitute var val val => Substitute var val (Predicate val) where
  substitute env = fmap (substitute env)

-- | Find the free variables inside a predicate.
instance (Eq var, FreeVars var val) =>  FreeVars var (Predicate val) where
  freevars = foldr (\v vars -> freevars v `union` vars) []

-- | Find the free variables inside a value.
instance Eq var =>  FreeVars var (Value ext var var) where
  freevars (Variable _ v) = [v]
  freevars (Thunk _ e) = freevars e
  freevars _ = []

-- | Substitute variables inside a named value, ignore extensions.
-- This substitution does not avoid variable capture.
instance Eq var => Substitute var (Value ext var var) (Value ext var var) where
  substitute env (Variable x var) = fromMaybe (Variable x var) (env var)
  substitute env (Thunk x e) = Thunk x (substitute env e)
  substitute _ v = v

-- | Substitute variables inside an expression, ignores extensions.
-- This substitution does not avoid variable capture.
instance
  Eq var => Substitute var (Value ext var var) (Expression ext var var)
  where
   substitute env = go where
     go (Parallel x e1 e2)    = Parallel x (go e1) (go e2)
     go (Sequential x e1 e2)  = Sequential x (go e1) (go e2)
     go (Choice x e1 p e2)    = Choice x (go e1) p (go e2)
     go (Iteration x e)       = Iteration x (go e)
     go (Lambda x var e)      = Lambda x var (substitute (protect var env) e)
     go (Application x e v) = Application x (go e) (substitute env v)
     go (Filter x p)          = Filter x (substitute env p)
     go (Modification x f v)  =
       Modification x (substitute env f) (substitute env v)
     go (Duplication x)       = Duplication x
     go (Force x v)           = Force x (substitute env v)
     go (Produce x v)         = Produce x (substitute env v)
     go (To x e1 var e2)      =
       To x (substitute env e1) var (substitute (protect var env) e2)
     go (Expression x)        = Expression x
     protect var env' v | v ==  var = Nothing
                        | otherwise = env' v

-- | Free variables for named terms.
instance Eq var => FreeVars var (Expression ext var var) where
  freevars (Parallel _ e1 e2) = freevars e1 `union` freevars e2
  freevars (Sequential _ e1 e2) = freevars e1 `union` freevars e2
  freevars (Choice _ e1 _ e2) = freevars e1 `union` freevars e2
  freevars (Iteration _ e) = freevars e
  freevars (Lambda _ var e) = freevars e \\ [var]
  freevars (Application _ e v) = freevars e `union` freevars v
  freevars (Filter _ p) = freevars p
  freevars (Modification _ f v) = freevars f `union` freevars v
  freevars (Duplication _) = []
  freevars (Force _ v) = freevars v
  freevars (Produce _ v) = freevars v
  freevars (To _ e1 var e2) = freevars e1 `union` (freevars e2 \\ [var])
  freevars (Expression _) = []



-- | Collapse a Program into a single Expression.
-- A program containing top level expressions and a main expression is
-- collapsed into a single expression by substituting the toplevel expression
-- into the main expression.
-- Note: if the top-level expressions are cyclic (mutually reference
-- each-other this function may not terminate).
xcollapsed
  :: Eq var
  => (Expression ext var var -> Value ext var var) -- ^ A way to make thunks
  -> Program ext var var            -- ^ The 'Program' to collapse
  -> Expression ext var var
xcollapsed xthunk (MkProgram toplevels expr) =
  let lookup' var = fmap xthunk (lookup var toplevels')
      toplevels' = [(var,substitute lookup' tp) | (var,tp) <- toplevels]
  in substitute lookup' expr


-- | Smart constructor for if-then-else.
--
--   If then else has no actual constructor, instead it is implemented as:
--
--   > if a then b else c == (a; b) & (~a; c)
xifthenelse
  :: XParallel ext var lvar         -- ^ 'Parallel' extension field for @ext@
  -> XSequential ext var lvar       -- ^ 'Sequential' extension field for @ext@
  -> XFilter ext var lvar           -- ^ 'Filter' extension field for @ext@
  -> Predicate (Value ext var lvar) -- ^ The 'Predicate' to check
  -> Expression ext var lvar        -- ^ branch if true
  -> Expression ext var lvar        -- ^ branch if false
  -> Expression ext var lvar
xifthenelse xp xs xf predicate prog1 prog2 =
  (Parallel xp
   (Sequential xs (Filter xf predicate) prog1)
   (Sequential xs (Filter xf (Negation predicate)) prog2))


-- | Fold over a 'Value'.
-- Analogously, convert to the Church-encoding.
xfoldV
  :: (XUnit ext var lvar -> a)
     -- ^ 'Unit' case
  -> (XThunk ext var lvar -> Expression ext var lvar -> a)
     -- ^ 'Thunk' case' 
  -> (XLabel ext var lvar -> String -> a)
     -- ^ 'Label' case
  -> (XLiteral ext var lvar -> Integer -> a)
     -- ^ 'Literal' case  
  -> (XVariable ext var lvar -> var -> a)
     -- ^ 'Variable' case
  -> Value ext var lvar
  -> a
xfoldV xunit xthunk xlabel xliteral xvar =
  let go (Unit x) = xunit x
      go (Thunk x e) = xthunk x e
      go (Label x l) = xlabel x l
      go (Literal x l) = xliteral x l
      go (Variable x var) = xvar x var
  in go
-- | Fold over an 'Expression'.
-- Analogously, convert to the Church-encoding.
xfold
  :: (XDuplication ext var lvar -> a)
     -- ^ 'Duplication' case
  -> (XFilter ext var lvar -> Predicate (Value ext var lvar) -> a)
     -- ^ 'Filter' case
  -> (XModification ext var lvar -> Value ext var lvar -> Value ext var lvar -> a)
     -- ^ 'Modification' case
  -> (XParallel ext var lvar -> a -> a -> a)
     -- ^ 'Parallel' case
  -> (XSequential ext var lvar -> a -> a -> a)
     -- ^ 'Sequential' case
  -> (XChoice ext var lvar -> a -> Double -> a -> a)
     -- ^ 'Choice' case
  -> (XIteration ext var lvar -> a -> a)
     -- ^ 'Iteration' case
  -> (XLambda ext var lvar -> lvar -> a -> a)
     -- ^ 'Lambda' case
  -> (XApplication ext var lvar -> a -> Value ext var lvar -> a)
     -- ^ 'Application' case
  -> (XForce ext var lvar -> Value ext var lvar -> a)
  -> (XProduce ext var lvar -> Value ext var lvar -> a)
  -> (XTo ext var lvar  -> a -> lvar -> a -> a) 
  -> (XExpression ext var lvar -> a)
     -- ^ 'Expression' case
  -> Expression ext var lvar
  -> a
xfold xdup xfil xmod xpar xseq xchoice xiter xlam xapp xforce xprod xto xexpr =
  let go (Duplication x) = xdup x
      go (Filter x p) = xfil x p
      go (Modification x f n) = xmod x f n
      go (Parallel x e1 e2) = xpar x (go e1) (go e2)
      go (Sequential x e1 e2) = xseq x (go e1) (go e2)
      go (Choice x e1 p e2) = xchoice x (go e1) p (go e2)
      go (Iteration x e) = xiter x (go e)
      go (Lambda x var body) = xlam x var (go body)
      go (Application x e v) = xapp x (go e) v
      go (Force x v) = xforce x v
      go (Produce x v) = xprod x v
      go (To x e1 var e2) = xto x (go e1) var (go e2)
      go (Expression x) = xexpr x
  in go

-- | Pretty print a program if we can print its expressions.
prettyProgram
  :: (Pretty var, Pretty (Expression ext lvar var))
  => Program ext lvar var -> PP.Doc
prettyProgram (MkProgram toplevels main_expression) =
    let prettyTL ident expr =
          pretty ident <+> PP.equals <+> PP.align (pretty expr)
    in PP.vsep [ prettyTL ident expr | (ident,expr) <- toplevels ]
       PP.<$>
       pretty main_expression

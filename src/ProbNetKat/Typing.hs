{- |

Module:      ProbNetKat.Typing
Description: Types for ProbNetKat with lambdas
Maintainer:  alexander.vandenbroucke@kuleuven.be

This module implements types ('Type') and type checking ('check') for
ProbNetKat programs, and defines the type of expressions annotated with
types ('Expression').

-}

{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ViewPatterns #-}

module ProbNetKat.Typing (
  -- * Type and Type Environments
  ValueType(Unit,Field,Literal,Thunk),
  ComputationType(Producer,Function),
  Env (Env),
  find,
  extend,
  size,
  -- * Typed Expressions
  Typed,
  HType(..), HVTy, HCTy,
  Value,
  Expression,
  Program,
  typeOfV,
  typeOf,
  -- * Type Checking
  (=:=), (>>=:=), marker,
  checkV, check,
  -- * Smart Constructors
  unit, label, literal, thunk, variable,
  duplication, ProbNetKat.Typing.filter, modification, produce, sequential,
  parallel, choice, iteration, iteration_, lambda, force, force_, to,
  application, application_
)
where

import           Control.Monad.Except (MonadError(throwError))
import           Data.Coerce
import           ProbNetKat.Syntax as S hiding (Value,Expression,Program,
                                               Unit,Literal,Thunk)
import           GHC.Generics hiding (to)

import qualified ProbNetKat.Syntax as S
import qualified ProbNetKat.Syntax.Simple as Simple
import qualified ProbNetKat.Syntax.Unparse as U
import           ProbNetKat.Result
import           Text.Pretty (Pretty(pretty,prettyPrec),parens)
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import           Text.PrettyPrint.ANSI.Leijen ((</>))


-------------------------------------------------------------------------------
-- Type and Type Environments

-- | Value types.
--
-- > S = unit | F | L | T C
--
data ValueType
  = Unit
    -- ^ Unit type
  | Field
    -- ^ Field (header) type
  | Literal
    -- ^ Literal type
  | Thunk !ComputationType
    -- ^ Thunk type, with the type of the suspended computation
  deriving (Show, Eq, Generic)

instance Pretty ValueType where
  prettyPrec _ Unit = PP.string "unit"
  prettyPrec _ Field = PP.string "F"
  prettyPrec _ Literal = PP.string "L"
  prettyPrec p (Thunk ct) = parens (p < 10) $
    PP.string "Tk" </> prettyPrec 9 ct

-- | Computation types.
--
-- > T = S -> T | P S
data ComputationType
  = Function !ValueType !ComputationType
    -- ^ Function type, with argument and result type
  | Producer !ValueType
    -- ^ Producer type, with the type of the produced value
  deriving (Show, Eq, Generic)

instance Pretty ComputationType where
  prettyPrec p (Function vt ct) = parens (p < 11) $
    prettyPrec 10 vt </> PP.string "->" </> prettyPrec 11 ct
  prettyPrec p (Producer vt) = parens (p < 10) $
    PP.string "P" </> prettyPrec 9 vt

-- | Typing environments.
newtype Env var = Env [(var,ValueType)] deriving (Show, Semigroup, Monoid)

-- | Find a variable in the typing environment.
find :: Eq var => var -> Env var -> Maybe ValueType
find v (Env env) = lookup v env

-- | Add a variable assignment to the typing environment.
extend :: var -> ValueType -> Env var -> Env var
extend v t (Env env) = Env ((v,t):env)

-- | Get the size of the environment
size :: Env var -> Int
size (Env env) = length env

-------------------------------------------------------------------------------
-- Typed Expressions

-- | Extension signature of 'TypedExpressions'.
data Typed

-- | Higher-order wrapper around a type.
newtype HType t var lvar = HType t deriving (Eq,Ord,Show,Pretty)

-- | Higher-order wrapper around a 'ValueType'.
type HVTy = HType ValueType

-- | Higher-order wrapper around a 'ComputationType'.
type HCTy = HType ComputationType

type instance XUnit Typed = HVTy
type instance XThunk Typed = HVTy
type instance XLabel Typed = HVTy
type instance XLiteral Typed = HVTy
type instance XVariable Typed = HVTy

type instance XDuplication Typed = HCTy
type instance XFilter Typed = HCTy
type instance XModification Typed = HCTy
type instance XParallel Typed = HCTy
type instance XSequential Typed = HCTy
type instance XChoice Typed = HCTy
type instance XIteration Typed = HCTy
type instance XLambda Typed = HCTy
type instance XApplication Typed = HCTy
type instance XForce Typed = HCTy
type instance XProduce Typed = HCTy
type instance XTo Typed = HCTy
type instance XExpression Typed = Simple.XClosed

-- | Values that are annotated with a type.
type Value = S.Value Typed

-- | Expressions that are annotated with a type.
type Expression = S.Expression Typed

-- | Expressions that are annotated with a type.
type Program = S.Program Typed

instance (Pretty var, Pretty lvar) => Pretty (Value var lvar) where
  prettyPrec p v = U.docOfV (U.unparseV v) p

instance (Pretty var, Pretty lvar) => Pretty (Expression var lvar) where
  prettyPrec p expr = U.docOf (U.unparse expr) p

instance (Pretty var, Pretty lvar) => U.Unparse Typed var lvar where
  unparseV =
    (xfoldV
     (const U.unit)
     (const $ U.thunk . U.unparse)
     (const U.label)
     (const U.literal)
     (const U.variable))
  unparse =
    (xfold
     (const U.duplication)
     (const $ U.filter . U.unparseP)
     (const $ \f v -> U.modification (U.unparseV f) (U.unparseV v))
     (const U.parallel)
     (const U.sequential)
     (const U.choice)
     (const U.iteration)
     unparseLambda
     (const $ \e v -> U.application e (U.unparseV v))
     (const $ U.force . U.unparseV)
     (const $ U.produce . U.unparseV)
     (const   U.to)
     Simple.absurdX)

instance (Pretty var, Pretty lvar) => Pretty (Program var lvar) where
   pretty = prettyProgram

unparseLambda
  :: Pretty lvar
  => HCTy var lvar -> lvar -> U.Expression var lvar -> U.Expression var lvar
unparseLambda (HType (Function t _)) var expr =
  let docp p = parens (p < 10) $
        PP.backslash <> pretty var <> PP.colon <> pretty t <> PP.dot
        </> U.docOf expr 10
  in S.Lambda (U.HDoc docp) var expr
unparseLambda _ var expr = U.lambda var expr

-- | Get the 'Type' of a 'Value'.
typeOfV :: Value var lvar -> ValueType
typeOfV =
  let prj1 s _     = coerce s
  in S.xfoldV coerce prj1 prj1 prj1 prj1

-- | Get the 'Type' of an 'Expression'.
typeOf :: Expression var lvar -> ComputationType
typeOf =
  let prj1 s _     = coerce s
      prj2 s _ _   = coerce s
      prj3 s _ _ _ = coerce s
  in (S.xfold
      coerce prj1 prj2 prj2 prj2 prj3 prj1 prj2 prj2 prj1 prj1 prj3
      Simple.absurdX)


-------------------------------------------------------------------------------
-- Type Checking

-- | Throw an error if the two arguments are unequal.
(=:=) :: (Pretty a, Eq a) => a -> a -> Result ()
(=:=) x y = when (x /= y) (throwError msg) where
  msg = pretty x </> PP.string "should be equal to" </> pretty y
infix 4 =:=

-- | Bind the result of the monad on the left and compare it with the second
-- argument.
(>>=:=) :: (Pretty a, Eq a) => Result a -> a -> Result ()
m >>=:= x = m >>= (=:= x)
infix 1 >>=:=

-- | This function marks which term is currently being checked.
marker :: Pretty a => a -> Result b -> Result b
marker term checker = catchError checker $ \msg -> throwError $ 
    msg PP.<> PP.comma PP.<$>
    PP.string "* while checking" </>
    PP.indent 2 (pretty term) PP.<$>
    PP.hardline
                                            
  
-- | Check that predicates are well typed.
checkPredicate
  :: (Pretty var, Eq var)
  => Env var -> Predicate (Value var var) -> Result ()
checkPredicate env p0 = marker (U.docOfP p0 11) $ case p0 of
  Drop -> return ()
  Skip -> return ()
  Test f v -> do
    checkV env f >>=:= Field
    checkV env v >>=:= Literal
  Disjunction p1 p2 -> do
    checkPredicate env p1
    checkPredicate env p2
  Conjunction p1 p2 -> do
    checkPredicate env p1
    checkPredicate env p2
  Negation p -> checkPredicate env p

-- | Check if a 'Value' is well-typed.
--
-- If the expression @v@ is ill-typed with respect to the environment @env@,
-- @checkV env v@ produces 'Nothing'.
-- Otherwise it produces @'Just' t@ where @t@ is the 'ValueType' of the
-- 'Value' v.
checkV
  :: (Pretty var, Eq var)
  => Env var -> Value var var -> Result ValueType
checkV env v0 = marker v0 $ case v0 of
  S.Unit (HType t) -> t =:= Unit >> return Unit
  S.Thunk (HType t) e -> do
    ct <- check env e
    t =:= Thunk ct
    return (Thunk ct)
  Label (HType t) _ -> t =:= Field >> return Field
  S.Literal (HType t) _ -> t =:= Literal >> return Literal
  Variable (HType t) var ->
    case find var env of
      Just t' -> t' =:= t >> return t
      Nothing ->
        throwError $ PP.string "unbound term variable: " </> pretty var

-- | Check if an 'Expression' is well-typed.
--
-- If the expression @e@ is ill-typed with respect to the environment @env@,
-- @check env e@ produces 'Nothing'.
-- Otherwise it produces @'Just' t@ where @t@ is the 'Type' of the
-- 'Expression' e.
check
  :: (Pretty var, Eq var)
  => Env var -> Expression var var -> Result ComputationType
check env e0 = marker e0 $ case e0 of
  Duplication (HType t) -> t =:= Producer Unit >> return t
  Filter (HType t) p -> do
    t =:= Producer Unit
    checkPredicate env p
    return t
  Modification (HType t) f v -> do
    t =:= Producer Unit
    Field <- checkV env f
    Literal <- checkV env v
    return t
  Sequential (HType t) e1 e2 -> do
    _  <- check env e1
    check env e2 >>=:= t
    return t
  Parallel (HType t) e1 e2 -> do
    check env e1 >>=:= t
    check env e2 >>=:= t
    return t
  Choice (HType t) e1 _ e2 -> do
    check env e1 >>=:= t
    check env e2 >>=:= t
    return t
  Iteration (HType t) e -> do
    check env e >>=:= Producer Unit
    t =:= Producer Unit
    return t
  Lambda (HType t@(Function tArg tResult)) var body -> do
    check (extend var tArg env) body >>=:= tResult
    return t
  e@(Lambda (HType t) _ _) -> throwError msg where
    msg =
      PP.string "lambda-abstraction" </> pretty e
      </> PP.string "with non-function type:" </> pretty t
  Application (HType t) e v -> do
    tFun <- check env e
    case tFun of 
      Function tArg tResult -> do
        checkV env v >>=:= tArg
        tResult =:= t
        return t
      _ -> throwError $
        PP.string "expected function type, but found:" </> pretty tFun
  Force (HType t) v -> do
    checkV env v >>=:= Thunk t
    return t
  Produce (HType t) v -> do
    tv <- checkV env v
    t =:= Producer tv
    return t
  To (HType t) e1 var e2 -> do
    tProd <- check env e1
    case tProd of
      Producer t1 -> do
        check (extend var t1 env) e2 >>=:= t
        return t
      _ -> throwError $
        PP.string "expected producer type, but found:" </> pretty tProd
  S.Expression x -> Simple.absurdX x


-------------------------------------------------------------------------------
-- Smart Constructors


-- | Unit value.
unit :: Value var lvar
unit = S.Unit (HType Unit)

-- | Label value.
label :: String -> Value var lvar
label str = S.Label (HType Field) str

-- | Literal value.
literal :: Integer -> Value var lvar
literal n = S.Literal (HType Literal) n

-- | Thunk value.
thunk :: Expression var lvar -> Value var lvar
thunk e = S.Thunk (HType $ Thunk $ typeOf e) e

-- | Variable value
variable :: ValueType -> var -> Value var lvar
variable t var = S.Variable (HType t) var

-- | Typed duplication
duplication :: Expression var lvar
duplication = S.Duplication (HType $ Producer Unit)

-- | Typed Filter
filter :: Predicate (Value var lvar) -> Expression var lvar
filter p = S.Filter (HType $ Producer Unit) p

-- | Typed modification
modification :: Value var lvar -> Value var lvar -> Expression var lvar
modification f n = S.Modification (HType $ Producer Unit) f n

-- | Typed produce.
produce :: Value var lvar -> Expression var lvar
produce v = S.Produce (HType (Producer (typeOfV v))) v

-- | Typed parallel.
sequential
  :: Expression var lvar -> Expression var lvar -> Expression var lvar
sequential e1 e2 = S.Sequential (HType (typeOf e2)) e1 e2

-- | Typed parallel composition.
parallel
  :: Expression var lvar -> Expression var lvar -> Expression var lvar
parallel e1 e2 = S.Parallel (HType (typeOf e1)) e1 e2

-- | Typed parallel.
choice
  :: Expression var lvar
  -> Double
  -> Expression var lvar
  -> Expression var lvar
choice e1 p e2 = S.Choice (HType (typeOf e1)) e1 p e2

-- | Typed iteration
iteration :: Expression var lvar -> Result (Expression var lvar)
iteration e@(typeOf -> Producer Unit) =
  return (S.Iteration (HType $ typeOf e) e)
iteration _ =
  throwError (PP.string "iteration: argument is not a unit producer.")

-- | Typed iteration
--
-- __WARNING__: This function is partial.
iteration_ :: Expression var lvar -> Expression var lvar
iteration_ e = result id (error . show) (iteration e)

-- | Typed lambda abstraction.
lambda :: ValueType
  -> lvar
  -> Expression var lvar
  -> Expression var lvar
lambda t var body = S.Lambda (HType $ Function t (typeOf body)) var body

-- | Typed force
force :: Value var lvar -> Result (Expression var lvar)
force v@(typeOfV -> Thunk t) = return (S.Force (HType t) v)
force _ = throwError (PP.string "force: argument is not a thunk")

-- | Typed force
--
-- __WARNING__: This function is partial.
force_ :: Value var lvar -> Expression var lvar
force_ v = result id (error . show) (force v)

-- | Typed application
application
  :: Expression var lvar -> Value var lvar -> Result (Expression var lvar)
application e@(typeOf -> Function tArg t) v@(typeOfV -> tVal) | tArg == tVal =
  return (S.Application (HType t) e v)
application _ _ =
  throwError (PP.string "application: invalid arguments.")

-- | Typed application
--
-- __WARNING__: This function is partial
application_
  :: Expression var lvar -> Value var lvar -> Expression var lvar
application_ e v = result id (error . show) (application e v)

-- | Typed to.
to :: Expression var lvar -> lvar
  -> Expression var lvar
  -> Expression var lvar
to e1 var e2 = To (HType (typeOf e2)) e1 var e2

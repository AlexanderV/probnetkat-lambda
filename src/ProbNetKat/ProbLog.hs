{- | 

Module: ProbNetKat.ProbLog
Description: ProbLog code generation
Maintainer: alexander.vandenbroucke@kuleuven.be

Translate a ProbNetKAT expression into ProbLog.

The translation is implemented by 'translatePredicate' and
'translateExpression'. These translate to ProbLog sentences and (one or more)
clauses respectively.

These functions assume that certain predicates are predefined:
@drop/2@, @skip/2@, @test/4@ and @modify/4@. The last two arguments to these
functions are always the input and output packet histories. In the case
of @test/4@ and @modify/4@, the first two arguments are the field name and
field value respectively.

The translations are build inside a 'Translate' monad which encapsulates
the necessary state. It can be evaluated with 'runTranslate'.

Example for some 'Expression' e:

> runTranslate (translateExpression e 4 "In" "Out")

translates an expression with 4-times unfolding of Kleene-star (*),
input variable "In" and output variable "Out".

-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}

module ProbNetKat.ProbLog (
  -- * Translation functions
  translatePredicate, translateExpression, translateProgram,
  inlined,
  -- * Translate monad
  Translate(runTranslate_),
  runTranslate)
where

import           Control.Monad.State (StateT, evalStateT, get, put)
import           Control.Monad.Writer (Writer, runWriter, tell)
import           Data.List (intersperse)
import           Data.String (IsString(fromString))
import           Data.Text.Lazy (Text)
import qualified Data.Text.Lazy.Builder as Text
import qualified Data.Text.Lazy.Builder.Int as Text
import qualified Data.Text.Lazy.Builder.RealFloat as Text
import           ProbNetKat.Syntax hiding (Value,Expression,Program)
import qualified ProbNetKat.Syntax (Expression(Expression))
import           ProbNetKat.Syntax.Simple (
  Value,Expression,Program,absurdX,thunk)
import qualified Data.HashMap.Strict as HashMap


-- | Inline the definitions of all variables in the main expression.
--
-- This also recursively inlines the variables in other top level definitions.
inlined :: Program String String -> Expression String String
inlined (MkProgram toplevels expr0) = 
  let hm = HashMap.fromList toplevels
      fromMap var = thunk <$> HashMap.lookup var hm
      go expr =
        let expr' = substitute fromMap expr
        in if expr == expr' then expr else go expr'
  in go expr0

-- | Translate a 'Predicate'.
-- The result is a ProbLog sentence that checks the predicate.
translatePredicate ::
     Predicate (Value String String)
     -- ^ The predicate to translate.
  -> Text.Builder
     -- ^ The input packet history variable.
  -> Text.Builder 
translatePredicate p phIn = case p of
    Drop -> "drop(" <> phIn <> ")"
    Skip -> "skip(" <> phIn <> ")"
    Test (Label _ field) (Literal _ value) -> 
      "test(" <>
      mintersperse "," [Text.fromString field, Text.decimal value, phIn]
      <> ")"
    Test _ _ ->
      error "translatePredicate: variables are not supported."
    Conjunction p1 p2 ->
      translatePredicate p1 phIn <> "," <> translatePredicate p2 phIn
    Disjunction p1 p2 ->
      "(" <>
      translatePredicate p1 phIn <> " ; " <> translatePredicate p2 phIn
      <> ")"
    Negation predicate -> "not(" <> translatePredicate predicate phIn <> ")"


-- | Translate an 'Expression'
--
-- The predicate is translated inside the 'Translate' monad. This function
-- may create new fresh variables, facts or predicates and may generate
-- clauses. In fact, every expression generates at least one clause.
--
-- The result of the monad is a ProbLog atom that simulates the effect
-- of the expression on a packet history when it is called.
--
-- The translation of a Kleene-star unfolds the starred expression a given
-- number of times:
--
-- > e^0     = skip
-- > e^(n+1) = skip & (e; e^n)
--
-- translating as it goes.
translateExpression ::
     Expression String String
     -- ^ The expression to translate.
  -> Int
     -- ^ The depth to which a Kleene-star should be unfolded.
  -> Text.Builder
     -- ^ The input packet history variable
  -> Text.Builder
     -- ^ The output packet history variable
  -> Translate Text.Builder
translateExpression expr n phIn phOut = case expr of
  -- It is useful to maintain the invariant that the result returned by
  -- translateExpression is an atom referring to a fresh predicate.
  Filter _ p ->
    clause phIn phOut $
      translatePredicate p phIn <> ", " <> phIn <> " = " <> phOut
  Modification _ (Label _ field) (Literal _ value) -> clause phIn phOut $
    "modify(" <>
    mintersperse "," [Text.fromString field, Text.decimal value, phIn, phOut]
    <> ")"
  Modification{} ->
    error "translateExpression: variables are not supported."
  Duplication _ -> clause phIn phOut $ "dup(" <> phIn <> "," <> phOut <> ")"
  Parallel _ e1 e2 ->
    "(" <>
    translateExpression e1 n phIn phOut
    <> " ; " <>
    translateExpression e2 n phIn phOut
    <> ")"
    >>= clause phIn phOut
  Sequential _ e1 e2 -> do
    v <- freshVar
    t1 <- translateExpression e1 n phIn v
    t2 <- translateExpression e2 n v phOut
    clause phIn phOut (t1 <> ", " <> t2)
  Choice _ e1 p e2 -> do
    f <- freshFact p
    t1 <- translateExpression e1 n phIn phOut
    t2 <- translateExpression e2 n phIn phOut
    multibody phIn phOut [
                 f <>  ", " <> t1,
      "not(" <>  f <> "), " <> t2]
  Iteration _ e -> do
    let go 0 i o = clause i o (i <> " = " <> o)
        go m i o = do
          v <- freshVar
          -- note: ProbLog memoises the facts in this body, the simplest
          -- solution is to always translate the expression anew, ensuring
          -- that all the facts are fresh.
          t <- translateExpression e n i v
          next <- go (m-1) v o
          clause i o $ "(" <> i <> " = " <> o <> ") ; " <> t <> ", " <> next
    go n phIn phOut
  Lambda{} -> error "translateExpression: Lambdas are not supported."
  Application{} ->
    error "translateExpression: Applications are not supported."
  ProbNetKat.Syntax.Expression x -> absurdX x

-- | Translate a 'Program'
-- Runs 'translateExpression' on the 'inlined' version of the program.
translateProgram ::
     Program String String
     -- ^ The Program to translate
  -> Int
     -- ^ The depth to which a Kleene-start should be unfolded. 
  -> Text.Builder
     -- ^ The input packet history variable.
  -> Text.Builder
     -- ^ The output packet history variable.
  -> Translate Text.Builder
translateProgram = translateExpression . inlined

-- | Intersperse a monoidal value, then concatenate.
mintersperse :: Monoid m => m -> [m] -> m
mintersperse x = mconcat . intersperse x

-------------------------------------------------------------------------------
-- Translate monad

-- | The Translate monad encapsulates the state needed during translation.
--
-- It's unlikely that you will have to mess with this directly.
newtype Translate a = MkTranslate {
    runTranslate_ :: StateT Int (Writer Text.Builder) a
  }
  deriving (Functor,Applicative,Monad)

instance Semigroup m => Semigroup (Translate m) where
  f1 <> f2 = (<>) <$> f1 <*> f2

instance Monoid m => Monoid (Translate m) where
  mempty = return mempty
  mappend f1 f2 = mappend <$> f1 <*> f2

instance IsString s => IsString (Translate s) where
  fromString = return .fromString

-- | Run the 'Translate' monad, producing a result, and the generated text.
runTranslate :: Translate a -> (a,Text)
runTranslate translate =
  let state    = runTranslate_ translate
      writer   = evalStateT state 0
      (x,text) = runWriter writer
  in (x,Text.toLazyText text)

----- Clause generation

-- | Generate and output a fresh predicate that has multiple clauses.
--
-- Given a set of clause bodies @[b1,...,bn]@, an input history variable
-- @In@ and an output history variable @Out@, that are free in @b1,...,bn@,
-- this function generates the clauses:
--
-- > p(In,Out) :- b1.
-- > ...
-- > p(In,Out) :- bn.
--
-- where @p@ is a fresh predicate symbol.
multibody ::
     Text.Builder
     -- ^ Input packet history variable
  -> Text.Builder
     -- ^ Output packet history variable
  -> [Text.Builder]
     -- ^ A list  of clause bodies
  -> Translate Text.Builder
multibody phIn phOut bodies = do
  p <- freshPredicate
  let cHead = p <> "(" <> phIn <> "," <> phOut <> ")"
  MkTranslate $
    mapM_ (\body -> tell (cHead  <> " :- " <> body <> ".\n")) bodies
  return cHead

-- | Generate and output a fresh predicate that has asingle clause.
-- Given a clause body @b@, an input history variable @In@ and an output
-- history variable @Out@, that are free in @b@, this function generates
-- the clause @p(In,Out) :- b.@ where @p@ is a fresh predicate symbol.
clause ::
     Text.Builder
     -- ^ Input packet history variable
  -> Text.Builder
     -- ^ Output packet history variable
  -> Text.Builder
     -- ^ clause body
  -> Translate Text.Builder
clause phIn phOut body = multibody phIn phOut [body]


----- Fresh facts,vars and predicates

-- | Generate a fresh fact.
--
-- @freshFact p@ generates a fresh fact that is true with probability @p@.
-- Facts are prefixed with the letter @f@.
freshFact :: Double -> Translate Text.Builder
freshFact p = MkTranslate $ do
  i <- get
  put (i+1)
  let f = "f" <> Text.decimal i
  tell (Text.formatRealFloat Text.Fixed Nothing p <>  " :: " <> f <> ".\n")
  return f

-- | Generate a fresh fact.
--
-- @freshVar@ generates a resh variable. Variables are prefixed with the
-- letter @V@.
freshVar :: Translate Text.Builder
freshVar = MkTranslate $ do
  i <- get
  put (i+1)
  return ("V" <> Text.decimal i)

-- | Generate a fresh fact.
--
-- @freshPredicate@ generates a fresh predicate. Predicates are prefixed
-- with the letter letter @p@.
freshPredicate :: Translate Text.Builder
freshPredicate = MkTranslate $ do
  i <- get
  put (i+1)
  return ("p" <> Text.decimal i)



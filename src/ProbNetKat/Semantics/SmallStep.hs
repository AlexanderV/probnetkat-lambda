{- |

Module:      ProbNetKat.Semantics.SmallStep
Description: Small-step semantics for ProbNetKat with lambdas.
Maintainer:  alexander.vandenbroucke@kuleuven.be

This module implements the small step semantics for ProbNetKat with
lambda-abstractions.

== Example

>>> import Text.Trifecta.Parser (parseString)
>>> import Text.Trifecta.Result (Result(Success))
>>> import ProbNetKat.Syntax.Parser (expression)
>>> import ProbNetKat.Syntax.Unparse (pretty)
>>> let Success prog = parseString expression mempty "(\\x.x) (skip & drop)"
>>> pretty $ runFresh $ step (wrap "a" "b" prog) (MaxLen (Var (MkVar "b")))
"([\\x.x]_x1_b [skip & drop]_a_x1, maxlen(b))"
>>> pretty $ runFresh $ steps (wrap "a" "b" prog) (MaxLen (Var (MkVar "a")))
"(()_a_x5, maxlen(a U empty))"

-}

{-# LANGUAGE GADTs #-}

module ProbNetKat.Semantics.SmallStep (
  -- * Expressions with Context
  Expression,
  Contextual,
  Q.Var(MkVar),
  wrap,
  step,
  steps,
  Q.Query(..),
)
where

import qualified ProbNetKat.Semantics.Query as Q
import qualified ProbNetKat.Syntax as S
import qualified ProbNetKat.Syntax.Simple as S
import qualified ProbNetKat.Syntax.Simple as Simple
import           Text.Pretty (Pretty(pretty, prettyPrec),parens,ampersand)
import           Text.PrettyPrint.ANSI.Leijen ((<>),(</>))
import qualified Text.PrettyPrint.ANSI.Leijen as PP


-------------------------------------------------------------------------------
-- Expressions with Context

-- | A convenient synonym for 'Simple.Expression'.
type Expression = Simple.Expression String String

-- | A Contextual 'Contextual'.
-- A 'Contextual' has similar structure to 'S.Expression' from
-- "ProbNetKat.Syntax", but has an additional context, to delay evaluation.
data Contextual
  = Context Expression
    -- ^ Wrap an 'S.Expression'
  | Unit
    -- ^ Unit value
  | Lambda String Expression
    -- ^ Lambda abstraction value
  | Parallel Contextual Contextual
    -- ^ Parallel composition
  | Sequential Contextual Contextual
    -- ^ Sequential composition
  | Choice Contextual Double Contextual
    -- ^ Probabilistic Choice
  | Iteration Int Expression
    -- ^ Finite Iteration
  | Application Contextual Contextual
    -- ^ Application
  deriving (Eq,Show)

instance Pretty Contextual where
  prettyPrec _ (Context e) = PP.brackets (pretty e)
  prettyPrec _ Unit = PP.pretty ()
  prettyPrec p (Lambda x e) =
    parens (p < 10) (PP.backslash <> PP.string x <> PP.dot <> prettyPrec 10 e)
  prettyPrec p (Parallel e1 e2) =
    parens (p < 9) (prettyPrec 9 e1 </> ampersand </> prettyPrec 9 e2)
  prettyPrec p (Sequential e1 e2) =
    parens (p < 8) (prettyPrec 8 e1 </> PP.semi </> prettyPrec 8 e2)
  prettyPrec p (Choice e1 r e2) = parens (p < 7) $
    prettyPrec 7 e1 </> PP.angles (PP.double r) </> prettyPrec 7 e2
  prettyPrec p (Application e1 e2) =
    parens (p < 5) (prettyPrec 5 e1 <> PP.space <> prettyPrec 5 e2)
  prettyPrec p (Iteration n e) =
    parens (p < 4) (prettyPrec 4 e <> circonflex <> PP.int n)

-- | A 'PP.Doc' containing an accent circonflex.
circonflex :: PP.Doc
circonflex = PP.char '^'

-- | Wrap a syntactic 'S.Expression' in a Context with the given variables.
wrap :: Expression -> Contextual
wrap = Context

-- | Expand one iteration of a finite iteration
expand :: Int -> Expression -> Contextual
expand 0 _ = Unit
expand n t =
  Context (S.filter S.Skip) `Parallel` (Context t `Sequential` expand (n-1) t)

-- | Erase all contextual information
erase :: Contextual -> Expression
erase (Context e) = e
erase Unit                = S.filter S.Skip
erase (Lambda x e)        = S.lambda x e
erase (Parallel e1 e2)    = S.parallel (erase e1) (erase e2)
erase (Sequential e1 e2)  = S.sequential (erase e1) (erase e2)
erase (Choice e1 r e2)    = S.choice (erase e1) r (erase e2)
erase (Application e1 e2) = S.application (erase e1) (erase e2)
erase (Iteration n e)     = erase (expand n e)

-- | Check if a 'Contextual' is a value and return its context variables.
isValue :: Contextual -> Bool
isValue Unit = True
isValue (Lambda _ _) = True
isValue _ = False

-- qapp :: Q.Query -> Q.Query -> Q.Query
-- qapp = Q.Application

-- | Perform a single small-step evaluation step.
step :: Q.QueryLanguage l => Contextual -> l -> (Contextual, l)
step e0 q = case e0 of
  Context (S.Modification _ f (S.ValueLiteral v)) ->
    (Unit, Q.lam $ \x -> Q.app q (Q.assign f v x))
  Context (S.Filter _ S.Drop) -> (Unit, Q.lam $ \_ -> Q.app q Q.empty)
  Context (S.Filter _ S.Skip) -> (Unit, q)
  Context (S.Filter _ (S.Disjunction x y)) ->
    (Context (S.parallel (S.filter x) (S.filter y)), q)
  Context (S.Filter _ (S.Conjunction x y)) ->
    (Context (S.sequential (S.filter x) (S.filter y)), q)
  --
  Context (S.Lambda _ x e1) -> (Lambda x e1, q)
  --
  Context (S.Sequential _ e1 e2) ->
    (Sequential (Context e1) (Context e2), q)
  Sequential e1 e2
    | isValue e1 && isValue e2 -> (e1, q)
    | isValue e1 -> 
        let (e2',q') = step e2 q
        in (Sequential e1 e2',q')
    | otherwise ->
        let (e1',q') = step e1 q
        in (Sequential e1' e2,q')
  --
  Context (S.Parallel _ e1 e2) -> do
    let q' = Q.lam $ \x -> Q.lam $ \y -> Q.app q (x `Q.union` y)
    (Parallel (Context e1) (Context e2), q')
  Parallel Unit Unit -> 
    (Unit, Q.lam $ \x -> Q.app (Q.app q x) x)
  Parallel Unit e ->
    let (e',q') = step e q
    in (Parallel Unit e',q')
  Parallel e1 e2 ->
    let (e1',q') = step e1 q
    in (Parallel e1' e2,q')
  --
  Context (S.Choice _ e1 r e2) ->
    let q' =
          Q.lam $ \x ->
          Q.lam $ \y ->
          Q.mul r (Q.app q x) `Q.plus` Q.mul (1-r) (Q.app q y)
    in (Choice (Context e1) r (Context e2), q')
  Choice Unit _ Unit->
    (Unit, Q.lam $ \x -> Q.app (Q.app q x) x)
  Choice Unit r e ->
    let (e',q') = step e q
    in (Choice Unit r e',q')
  Choice e1 r e2 ->
    let (e1',q') = step e1 q
    in (Choice e1' r e2, q')
  --
  Context (S.Application _ e1 e2) -> 
    (Application (Context e1) (Context e2), q)
  Application (Lambda x t1) v2 ->
    let env y | y == x = Just (erase v2)
              | otherwise = Nothing
    in (Context (S.substitute env t1), q)
  Application e1 e2
    | isValue e1 -> 
        let (e2',q') = step e2 q
        in (Application e1 e2', q')
    | otherwise ->
        let (e1',q') = step e1 q
        in (Application e1' e2, q')
  --
  Context (S.Iteration _ e) -> (Iteration 20 e, q)
  Iteration n e
    | n == 0 -> (Unit, q)
    | otherwise ->
        let q' = Q.lam $ \x -> Q.lam $ \y -> Q.app q (x `Q.union` y)
        in (expand n e, q')

  --
  Context (S.Expression x) -> S.absurdX x
-- assumptions for step:
-- * parallel only has unit type
-- * negation/test not supported
-- * support for conjunction/disjunction is inefficient
-- * not checking if vars are eq in final sequence rule
-- * cannot have arbitrary value in final parallel or choice rule
-- * dup not supported
-- the query produced by the first skip should not be integrated over by
-- the application.
-- integration variable

-- | Perform as many small-step evaluations as possible.
steps :: Q.QueryLanguage l => Contextual -> l -> (Contextual,l)
steps x q
  | isValue x = (x, q)
  | otherwise = uncurry steps (step x q)

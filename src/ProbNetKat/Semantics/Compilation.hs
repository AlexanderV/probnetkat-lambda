{- |

Module:      ProbNetKat.Semantics.Compilation
Description: Compilation of ProbNetKat with lambdas to ProbNetKat.
Maintainer:  alexander.vandenbroucke@kuleuven.be

This module implements compilation from a probabilistic NetKAT program with
lambdas to a probabilistic NetKAT program without lambdas or any CBPV forms.

This final state is captured by the 'Probabilistic' extension of the syntax
which closes all non-standard NetKAT constructors.
-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}



module ProbNetKat.Semantics.Compilation (
  Probabilistic,
  Value, Expression,
  reduce, erase, compile
)
where

import           Control.Monad ((>=>))

import           Data.Bifunctor (bimap)
import           ProbNetKat.Result
import qualified ProbNetKat.Syntax as S
import           ProbNetKat.Syntax hiding (XUnit,Value,Expression,substitute)
import qualified ProbNetKat.Syntax.DeBruijn as DB
import           ProbNetKat.Syntax.Simple (
  XUnit(XU),XClosed,absurdX,simplify,simplifyV)
import qualified ProbNetKat.Syntax.Unparse as U
import           Text.Pretty
import qualified Text.PrettyPrint.ANSI.Leijen as PP
  
-- | Extension signature of probabilistic programs.
data Probabilistic

type instance S.XUnit Probabilistic = XClosed
type instance XThunk Probabilistic = XClosed
type instance XLabel Probabilistic = XUnit
type instance XLiteral Probabilistic = XUnit
type instance XVariable Probabilistic = XClosed

type instance XDuplication Probabilistic = XUnit
type instance XFilter Probabilistic = XUnit
type instance XModification Probabilistic = XUnit
type instance XParallel Probabilistic = XUnit
type instance XSequential Probabilistic = XUnit
type instance XChoice Probabilistic = XUnit
type instance XIteration Probabilistic = XUnit
type instance XLambda Probabilistic = XClosed
type instance XApplication Probabilistic = XClosed
type instance XForce Probabilistic = XClosed
type instance XProduce Probabilistic = XClosed
type instance XTo Probabilistic = XClosed
type instance XExpression Probabilistic = XClosed

-- | Probabilistic expressions
type Value = S.Value Probabilistic

-- | Probabilistic expressions
type Expression = S.Expression Probabilistic

instance Pretty (Value var lvar) where
  prettyPrec p v = U.docOfV (U.unparseV v) p

instance Pretty (Expression var lvar) where
  prettyPrec p e = U.docOf (U.unparse e) p

instance U.Unparse Probabilistic var lvar where
  unparseV =
    S.xfoldV absurdX absurdX (const $ U.label) (const $ U.literal) absurdX
  unparse expr = case expr of
    S.Duplication _ -> U.duplication
    S.Filter _ p -> U.filter (fmap U.unparseV p)
    S.Modification _ f n -> U.modification (U.unparseV f) (U.unparseV n)
    S.Parallel _ e1 e2 -> U.parallel (U.unparse e1) (U.unparse e2)
    S.Sequential _ e1 e2 -> U.sequential (U.unparse e1) (U.unparse e2)
    S.Choice _ e1 p e2 -> U.choice (U.unparse e1) p (U.unparse e2)
    S.Iteration _ e -> U.iteration (U.unparse e)
    S.Lambda xclosed _ _ -> absurdX xclosed
    S.Application xclosed _ _ -> absurdX xclosed
    S.Force xclosed _ -> absurdX xclosed
    S.Produce xclosed _ -> absurdX xclosed
    S.To xclosed _ _ _ -> absurdX xclosed
    S.Expression xclosed -> absurdX xclosed

-- | Reduce an 'DeBruijn.Expression'.
-- The reduction removes applications and sequencing as much as possible.
-- On a closed expression with a unit producer type, this effectively removes
-- all applications, lambda-abstractions, forces and sequencing, leaving
-- only standard probabilistic NetKAT and produce unit.
reduce :: DB.Expression var var -> Result (DB.Expression var var)
reduce expr@Duplication{} = return expr
reduce expr@Filter{} = return expr
reduce expr@Modification{} = return expr
reduce (Parallel XU e1 e2) = DB.parallel <$> reduce e1 <*> reduce e2
reduce (Sequential XU e1 e2) = DB.sequential <$> reduce e1 <*> reduce e2
reduce (Choice XU e1 p e2) = DB.choice <$> reduce e1 <*> pure p <*> reduce e2
reduce (Iteration XU e) = DB.iteration <$> reduce e
reduce (Lambda XU var e) = return (DB.lambda var e)
reduce (Application XU e v) = reduce e >>= \t -> case t of
  Lambda XU _ e' -> reduce (substitute v e')
  Sequential XU t1 t2 -> DB.sequential t1 <$> reduce (DB.application t2 v)
  Choice XU t1 p t2 -> do
    t1' <- reduce (DB.application t1 v)
    t2' <- reduce (DB.application t2 v)
    return (DB.choice t1' p t2')
  _ ->
    throwError (PP.string "reduce: ill-typed argument to application.")
reduce (Force XU (Thunk XU e)) = reduce e
reduce Force{} = throwError (PP.string "reduce: ill-formed argument to force.")
reduce expr@Produce{} = return expr
reduce (To XU e1 var e2) = reduce e1 >>= \t -> case t of
  Produce XU v ->
    reduce (substitute v e2)
  Filter{} -> DB.sequential t <$> reduce (substitute DB.unit e2)
  Modification{} ->
    DB.sequential t <$> reduce (substitute DB.unit e2)
  Duplication{} ->
    DB.sequential t <$> reduce (substitute DB.unit e2)
  Iteration{} -> DB.sequential t <$> reduce (substitute DB.unit e2)
  Sequential XU t1 t2 ->
    DB.sequential t1 <$> reduce (DB.to t2 var e2)
  Parallel XU t1 t2 ->
    DB.sequential (DB.parallel t1 t2) <$> reduce (substitute DB.unit e2)
  Choice XU t1 p t2 -> do
    t1' <- reduce (DB.to t1 var e2)
    t2' <- reduce (DB.to t2 var e2)
    return (DB.choice t1' p t2')
  _ ->
    throwError (PP.string "reduce: ill-formed argument to \"to\".")
reduce (S.Expression x) = absurdX x

-- | A wrapper around deBruijn indexed values, s.t. type-class instantiation
-- will choose the right instance.
newtype DB var lvar = MkDB { unDB :: DB.Expression var lvar }

instance S.Substitute Int (DB.Value var lvar) (DB var lvar) where
  substitute env (MkDB e) = MkDB (S.substitute env e)

-- | Substitute a deBruijn-indexed value for all zero distance variables in a
-- DeBruijn-indexed expression, and down shift all variables in the resulting
-- expression.
substitute
  :: DB.Value var lvar
  -> DB.Expression var lvar
  -> DB.Expression var lvar
substitute val = 
  let subst var | var == (0 :: Int) = Just val
                | otherwise = Nothing
  in DB.shiftE 0 (-1) . unDB . S.substitute subst . MkDB

eraseV :: DB.Value var var -> Result (S.Value Probabilistic var var)
eraseV (Label XU l) = return (S.Label XU l)
eraseV (Literal XU l) = return (S.Literal XU l)
eraseV Unit{} = throwError (PP.string "eraseV: unexpected unit value.")
eraseV Thunk{} = throwError (PP.string "eraseV: unexpected thunk value.")
eraseV Variable{} = throwError (PP.string "eraseV: unexpected variable.")

-- | Replace all occurences of @produce@ with @skip@.
erase :: DB.Expression var var -> Result (S.Expression Probabilistic var var)
erase e0 = case e0 of
  S.Duplication{}         -> return (S.Duplication XU)
  (S.Filter XU p)         -> S.Filter XU <$> traverse eraseV p
  (S.Modification XU f n) -> S.Modification XU  <$> eraseV f <*> eraseV n
  (S.Parallel XU e1 e2)   -> S.Parallel XU <$> erase e1 <*> erase e2
  (S.Sequential XU e1 e2) -> S.Sequential XU <$> erase e1 <*> erase e2
  (S.Choice XU e1 p e2)   -> S.Choice XU <$> erase e1 <*> pure p <*> erase e2
  (S.Iteration XU e)      -> S.Iteration XU <$> erase e
  S.Lambda{}              -> return (S.Filter XU S.Skip)
  S.Produce{}             -> return (S.Filter XU S.Skip)
  _ -> throwError (PP.string "erase: unexpected non-terminal.")

-- | Erase after reduction.
compile :: Eq var => DB.Expression var var -> Result (Expression var var)
compile = reduce >=> erase

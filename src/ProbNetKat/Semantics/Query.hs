{- |

Module:      ProbNetKat.Semantics.Query
Description: Query language for Probabilistic NetKAT programs.
Maintainer:  alexander.vandenbroucke@kuleuven.be

This module implements a modest query language that is to be run agains
probabilistic NetKAT programs.

-}

{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}

module ProbNetKat.Semantics.Query (
  Var(MkVar),
  Query(Var,Empty,MaxLen,Plus,Mul,Union,Comprehension,Lambda,Application),
  QueryLanguage(..),
  Fresh(MkFresh,_runFresh),
  runFresh,
  fresh,
  substitute
  )
where

import           Control.Monad.State
import           ProbNetKat.Syntax (Field)
import           Text.Pretty (Pretty(prettyPrec), parens)
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import           Text.PrettyPrint.ANSI.Leijen ((<>),(</>))

class QueryLanguage l where
  empty :: l
  assign :: Field -> Integer -> l -> l
  union :: l -> l -> l

  lam :: (l -> l) -> l
  app :: l -> l -> l

  mul  :: Double -> l -> l
  plus :: l -> l -> l


-------------------------------------------------------------------------------

-- | A wrapper around query variables
newtype Var = MkVar String deriving (Eq,Show)

-- | Query syntax
data Query
  = Var Var
  | Empty
  | MaxLen Query
  | Plus Query Query
  | Mul Double Query
  | Union Query Query
  | Comprehension Field Integer Query
  | Lambda Var Query
  | Application Query Query 
  deriving Show

instance Pretty Query where
  prettyPrec _ (Var (MkVar v)) = PP.string v
  prettyPrec _ Empty           = PP.string "empty"
  prettyPrec _ (MaxLen q)      = PP.string "maxlen" <> PP.parens (prettyPrec 10 q)
  prettyPrec p (Union q1 q2) =
    parens (p < 9) (prettyPrec 9 q1 </> unionDoc </> prettyPrec 9 q2)
  prettyPrec p (Plus q1 q2) =
    parens (p < 9) (prettyPrec 9 q1 </> plusDoc </> prettyPrec 9 q2)
  prettyPrec p (Mul r q) =
    parens (p < 8) (PP.double r </> timesDoc </> prettyPrec 8 q)
  prettyPrec _ (Comprehension f n q) = PP.braces $
    let pi' = piDoc <> assignment f n
    in comprehension  (ph pi') (ph piDoc) (prettyPrec 10 q)
  prettyPrec p (Lambda (MkVar x) q) =
    parens (p < 10) (PP.backslash <> PP.string x <> PP.dot <> prettyPrec 10 q)
  prettyPrec p (Application q1 q2) =
    parens (p < 5) (prettyPrec 5 q1 <> PP.space <> prettyPrec 5 q2)

unionDoc :: PP.Doc
unionDoc = PP.string "U"

plusDoc :: PP.Doc
plusDoc = PP.string "+"

timesDoc :: PP.Doc
timesDoc = PP.string "*"

piDoc :: PP.Doc
piDoc = PP.string "pi"

ph :: PP.Doc -> PP.Doc
ph packet = packet <> PP.string "::" <> PP.string "h"

comprehension :: PP.Doc -> PP.Doc -> PP.Doc -> PP.Doc
comprehension result element set =
  PP.braces $ PP.fillSep [result, PP.string "|", element, PP.string "in", set]

assignment :: Field -> Integer -> PP.Doc
assignment f n = PP.brackets (PP.string f </> PP.string "<-" </> PP.integer n)

instance QueryLanguage (Fresh Query) where
  empty = return Empty
  assign f v q = Comprehension f v <$> q
  union q1 q2  = Union <$> q1 <*> q2
  lam f = do
    x <- fresh
    Lambda x <$> f (return (Var x))
  app q1 q2 = Application <$> q1 <*> q2
  mul r q = Mul r <$> q
  plus q1 q2 = Plus <$> q1 <*> q2

-- | Substitute a variable in a 'Query'.
substitute :: Var -> Query -> Query -> Query
substitute v' q0 =
  let go (Var v)       = if v == v' then q0 else Var v
      go (Plus q1 q2)  = Plus (go q1) (go q2)
      go (Mul d q)     = Mul d (go q)
      go (Union q1 q2) = Union (go q1) (go q2)
      go (Comprehension f v q) = Comprehension f v (go q)
      go (MaxLen q)    = MaxLen (go q)
      go Empty         = Empty
      go (Lambda v q)  = Lambda v (if v == v' then q else go q)
      go (Application q1 q2) = Application (go q1) (go q2)
  in go

-------------------------------------------------------------------------------
-- A Fresh supply of variables.

-- | A monad that provides a fresh supply of variables.
newtype Fresh a =
  MkFresh
  {
    _runFresh :: State [Var] a
  }
  deriving (Functor,Applicative,Monad)

-- | Evaluate 'Fresh'.
runFresh :: Fresh a -> a
runFresh m =
  evalState (_runFresh m) [MkVar ('x':show x) | x <- [(1 :: Integer)..]]

-- | Get a fresh unused variable.
fresh :: Fresh Var
fresh = MkFresh $ do
  (v:vs) <- get
  put vs
  return v

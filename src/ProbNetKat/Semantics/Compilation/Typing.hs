{- |

Module:      ProbNetKat.Semantics.Compilation.Typing
Description: Restricted type checking for compilation
Maintainer:  alexander.vandenbroucke@kuleuven.be

Terms can only be compiled if the type of parallel composition is restricted
to 'T.Producer T.Unit'.
-}

module ProbNetKat.Semantics.Compilation.Typing (
  check, checkPar
)
where

import           ProbNetKat.Result
import           ProbNetKat.Syntax
import           ProbNetKat.Typing (HType(HType),(=:=))
import qualified ProbNetKat.Typing as T

import           Text.Pretty
import           Text.PrettyPrint.ANSI.Leijen ((</>))
import qualified Text.PrettyPrint.ANSI.Leijen as PP


-- | This function marks which term is currently being checked.
marker :: Pretty a => a -> Result b -> Result b
marker term checker = catchError checker $ \msg -> throwError $
    msg PP.<> PP.comma PP.<$>
    PP.string "* while compilation checking" </>
    PP.indent 2 (pretty term) PP.<$>
    PP.hardline

-- | Check if an 'Expression' is well-typed.
--
-- This is the case if it satisfies 'T.check', and the type of parallel
-- composition is restricted to 'T.Producer T.Unit'.
check
  :: (Eq var, Pretty var)
  => T.Env var -> T.Expression var var -> Result T.ComputationType
check env e0 = T.check env e0 >> checkPar env e0

checkV
  :: (Eq var, Pretty var)
  => T.Env var -> T.Value var var -> Result T.ValueType
checkV env (Thunk (HType t) e) = checkPar env e >> return t
checkV _ v = return (T.typeOfV v)

-- | Check that every Parallel Composition has type 'T.Producer T.Unit'.
checkPar
  :: (Eq var, Pretty var)
  => T.Env var -> T.Expression var var -> Result T.ComputationType
checkPar env e0 = marker e0 $ case e0 of
  Duplication{} -> return t0
  Modification{} -> return t0
  Filter _ p ->
    traverse (checkV env) p >> return t0
  Sequential _ e1 e2 ->
    checkPar env e1 >> checkPar env e2
  Parallel (HType t) e1 e2 -> do
    t =:= T.Producer T.Unit
    checkPar env e1 >> checkPar env e2
  Choice _ e1 _ e2 ->
    checkPar env e1 >> checkPar env e2
  Iteration _ e ->
    checkPar env e
  Lambda _ var body -> case t0 of
    T.Function tArg _ -> checkPar (T.extend var tArg env) body >> return t0
    T.Producer _ -> return t0
  Force _ v ->
    checkV env v >> return t0
  Produce _ v ->
    checkV env v >> return t0
  To _ e1 var e2 -> do
    T.Producer t1 <- checkPar env e1
    checkPar (T.extend var t1 env) e2
  Application _ e v ->
    checkPar env e >> checkV env v >> return t0
  Expression _ -> return t0
  where t0 = T.typeOf e0
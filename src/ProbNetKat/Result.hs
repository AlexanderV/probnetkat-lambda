{- |

Module:      ProbNetKat.Result
Description: Result type for general use.
Maintainer:  alexander.vandenbroucke@kuleuven.be

This module defines a Result monad, that can be used to give information
about operations that may fail, such as unification.

-}

{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveFunctor, DeriveFoldable, DeriveTraversable #-}
{-# LANGUAGE DeriveGeneric #-}

module ProbNetKat.Result (
  Result(..),
  MonadError(throwError,catchError),
  isSuccess,
  isFailure,
  when,
  result
)
where

import           Control.Monad (when)
import           Control.Monad.Except (MonadError(throwError,catchError))
import           GHC.Generics
import           Text.Pretty (Pretty(prettyPrec), parens)
import           Text.PrettyPrint.ANSI.Leijen ((</>))
import qualified Text.PrettyPrint.ANSI.Leijen as PP


-- | A result.
data Result a
  = Success a       -- ^ A successful result
  | Failure PP.Doc  -- ^ A failed result, with an error message
  deriving (Show,Functor,Foldable,Traversable,Generic)

instance Applicative Result where
  pure = return
  Failure d <*> _ = Failure d
  _ <*> Failure d = Failure d
  Success f <*> Success x = Success (f x)

instance Monad Result where
  return = Success
  Failure d >>= _ = Failure d
  Success x >>= f = f x

instance MonadError PP.Doc Result where
  throwError = Failure
  catchError (Success a) _ = Success a
  catchError (Failure ex) hndl = hndl ex

instance Pretty a => Pretty (Result a) where
  prettyPrec p (Success a) = parens (p < 1) $
    PP.dullgreen (PP.string "Success") </> prettyPrec 0 a
  prettyPrec p (Failure doc) = parens (p < 1) $
    PP.red (PP.string "Failure: ") </> doc

-- | Returns True only if the result is succesful.
isSuccess :: Result a -> Bool
isSuccess (Success _) = True
isSuccess (Failure _) = False

-- | Returns True only if the result is a failure.
isFailure :: Result a -> Bool
isFailure = not . isSuccess

-- | Case analysis for 'Result'. If the result is @'Success' x@, the first
-- function applied to @x@, if @'Failure' msg@, the second function is applied
-- to @msg@.
result :: (a -> b) -> (PP.Doc -> b) -> Result a -> b
result onSuccess _onFailure (Success x) = onSuccess x
result _onSuccess onFailure (Failure msg) = onFailure msg

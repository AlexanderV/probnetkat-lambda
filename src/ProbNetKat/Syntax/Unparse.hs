{- | 

Module:      ProbNetKat.Syntax.Unparse
Description: pretty Printer for ProbNetKAT programs
Maintainer:  alexander.vandenbroucke@kuleuven.be

Pretty-printer for the ProbNetKat syntax. The result is similar to the
surface-syntax presented in "ProbNetKat.Syntax.Parser".

The pretty printer will only print parenthesis when necessary due to
precedence. The 'pretty' functions are not exact inverse of parsing in
"ProbNetKat.Syntax.Parser" since these the abstract 'Expression' data-type
does not support if-then-else directly, nor does it remember whitespace.
-}

{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module ProbNetKat.Syntax.Unparse (
  -- * Unparsed syntax
  Unparsed,
  HDoc(HDoc), hdoc,
  Value,
  Expression,
  docOfV, docOfP, docOfP', docOf,

  -- ** Smart Constructors
  unit, thunk, label, literal, variable,
  duplication,  filter, modification, parallel, sequential, choice,
  iteration, lambda, application, force, produce, to, expression,

  -- * Unparsing class
  Unparse(unparse,unparseV), unparseP
)
where

import           Data.Coerce
import           Prelude hiding (filter)
import           Text.Pretty
import           Text.PrettyPrint.ANSI.Leijen (Doc, (<>), (</>), (<+>))
import qualified Text.PrettyPrint.ANSI.Leijen as PP

import qualified ProbNetKat.Syntax as S
import           ProbNetKat.Syntax as S hiding (Expression,Value)


-- | Extension signature of 'UnparsedExpression'
data Unparsed

-- | 'HDoc' is a wrapper around functions that take a precedence and return
-- a 'Doc'
newtype HDoc var lvar = HDoc (Int -> Doc)

instance Pretty (HDoc var lvar) where
  prettyPrec p (HDoc f) = f p

instance Show (HDoc var lvar) where
  show = show . pretty

-- | Wrap a 'Doc' in a 'HDoc'
hdoc :: Doc -> HDoc var lvar
hdoc = HDoc . const

type instance XUnit Unparsed = HDoc
type instance XThunk Unparsed = HDoc
type instance XLabel Unparsed = HDoc
type instance XLiteral Unparsed = HDoc
type instance XVariable Unparsed = HDoc

type instance XDuplication Unparsed = HDoc
type instance XFilter Unparsed = HDoc
type instance XModification Unparsed = HDoc
type instance XParallel Unparsed = HDoc
type instance XSequential Unparsed = HDoc
type instance XChoice Unparsed = HDoc
type instance XIteration Unparsed = HDoc
type instance XLambda Unparsed = HDoc
type instance XApplication Unparsed = HDoc
type instance XForce Unparsed = HDoc
type instance XProduce Unparsed = HDoc
type instance XTo Unparsed = HDoc
type instance XExpression Unparsed = HDoc

-- | Values that are annotated with their Unparsing
type Value = S.Value Unparsed

-- | Expressions that are annotated with their Unparsing
type Expression = S.Expression Unparsed

-- | Get the Unparsing of a 'Predicate'
docOfP' :: (val -> Int -> Doc) -> Predicate val -> Int -> Doc
docOfP' docOfV' = go where
  go Drop _ = PP.string "drop"
  go Skip _ = PP.string "skip"
  go (Test field value) p =
    parens (p < 7) (docOfV' field 7 <+> PP.string "==" <+> docOfV' value 7)
  go (Disjunction predicate1 predicate2) p = parens (p < 9) $
    go predicate1 9 </> ampersand </> go predicate2 9
  go (Conjunction predicate1 predicate2) p = parens (p < 8)
    (go predicate1 8 <> PP.semi </> go predicate2 8)
  go (Negation predicate) p =
    parens (p < 4) (PP.string "~" <> go predicate 4)

-- | Get the Unparsing of a 'Predicate', assuming the values are pretty.
docOfP :: Pretty val => Predicate val -> Int -> Doc
docOfP = docOfP' pp where pp val p = prettyPrec p val

-- | Get the Unparsing of a 'Value'
docOfV :: Value var lvar -> Int -> Doc
docOfV (Unit d) = coerce d
docOfV (Thunk d _) = coerce d
docOfV (Label d _) = coerce d
docOfV (Literal d _) = coerce d
docOfV (Variable d _) = coerce d

-- | Get the Unparsing of an 'Expression'.
docOf :: Expression var lvar -> Int -> Doc
docOf =
  let prj0 s       = coerce s
      prj1 s _     = coerce s
      prj2 s _ _   = coerce s
      prj3 s _ _ _ = coerce s
  in S.xfold prj0 prj1 prj2 prj2 prj2 prj3 prj1 prj2 prj2 prj1 prj1 prj3 prj0


-- | Members of this class permit unparsing their syntax, i.e. they can
-- be turned into a syntax tree annotated with its own pretty printing.
class Unparse ext var lvar where
  unparseV :: S.Value ext var lvar -> Value var lvar
  -- ^ Annotate a value with its pretty printing
  unparse :: S.Expression ext var lvar -> Expression var lvar
  -- ^ Annotate a syntax tree with its pretty printing.

instance Unparse Unparsed var lvar where
  unparseV = id
  unparse = id

-- | Annotate the values inside a 'Predicate' with their pretty printing.
unparseP
  :: Unparse ext var lvar
  => Predicate (S.Value ext var lvar) -> Predicate (Value var lvar)
unparseP = fmap unparseV

-------------------------------------------------------------------------------
-- Smart Constructors

-- | Unparsed unit.
unit :: Value var lvar
unit = Unit (hdoc $ PP.string "unit")

-- | Unparsed thunk.
thunk :: Expression var lvar -> Value var lvar
thunk expr = Thunk (HDoc docp) expr where
  docp p = parens (p < 3) (PP.string "thunk" <+> PP.parens (docOf expr 11))

-- | Unparsed label.
label :: String -> Value var lvar
label l = Label (hdoc $ PP.string l) l

-- | Unparsed literal.
literal :: Integer -> Value var lvar
literal i = Literal (hdoc $ PP.integer i) i

-- | Unparsed variable.
variable :: Pretty var => var -> Value var lvar
variable var = Variable (hdoc $ pretty var) var

-- | Unparsed duplication.
duplication :: Expression var lvar
duplication = Duplication (hdoc $ PP.string "dup")

-- | Unparsed filter.
filter :: Predicate (Value var lvar) -> Expression var lvar
filter predicate =
  Filter (HDoc $ \p -> docOfP' docOfV predicate (min p 7)) predicate
  -- Predicates that are not negation, skip, drop or test will always get
  -- parentheses.

-- | Unparsed modification.
modification :: Value var lvar -> Value var lvar -> Expression var lvar
modification field value = Modification (HDoc docp) field value where
  docp p = parens (p < 7) $
    docOfV field 7 <+> PP.string "<-" <+> docOfV value 7

-- | Unparsed parallel composition.
parallel :: Expression var lvar -> Expression var lvar -> Expression var lvar
parallel expr1 expr2 = Parallel (HDoc docp) expr1 expr2 where
  docp p = parens (p < 9) $ docOf expr1 9 </> PP.string "&" </> docOf expr2 9

-- | Unparsed sequential composition.
sequential :: Expression var lvar -> Expression var lvar -> Expression var lvar
sequential expr1 expr2 = Sequential (HDoc docp) expr1 expr2 where
  docp p = parens (p < 8) $ docOf expr1 8 <> PP.string ";" </> docOf expr2 8

-- | Unparsed probabilistic choice.
choice
  :: Expression var lvar
  -> Double
  -> Expression var lvar
  -> Expression var lvar
choice expr1 prob expr2 = Choice (HDoc docp) expr1 prob expr2 where
  docp p = parens (p < 7) $
    docOf expr1 7 </> PP.angles (PP.double prob) </> docOf expr2 7

-- | Unparsed iteration.
iteration :: Expression var lvar -> Expression var lvar
iteration expr = Iteration (HDoc docp) expr where
  docp p = parens (p < 4) (docOf expr 4 <> PP.string "*")

-- | Unparsed lambda abstraction.
lambda :: Pretty lvar => lvar -> Expression var lvar -> Expression var lvar
lambda lvar expr = Lambda (HDoc docp) lvar expr where
  docp p = parens (p < 10) $
    PP.backslash <> pretty lvar <> PP.dot <> docOf expr 10

-- | Unparsed function application.
application
  :: Expression var lvar
  -> Value var lvar
  -> Expression var lvar
application expr val = Application (HDoc docp) expr val where
  docp p = parens (p < 5) (docOf expr 5 <> PP.space <> docOfV val 5)

-- | Unparsed force.
force :: Value var lvar -> Expression var lvar
force val = Force (hdoc $ PP.string "force" <> PP.parens (docOfV val 11)) val

-- | Unparsed produce.
produce :: Value var lvar -> Expression var lvar
produce val =
  Produce (hdoc $ PP.string "produce" <> PP.parens (docOfV val 11)) val

-- | Unparsed to.
to :: Pretty lvar
  => Expression var lvar -> lvar -> Expression var lvar -> Expression var lvar
to e1 lvar e2 = To (HDoc docp) e1 lvar e2 where
  docp p = parens (p < 6) $
    docOf e1 6 </> PP.string "to" </> pretty lvar <> PP.dot <> docOf e2 6

-- | Unparsed full expression.
expression :: Doc -> Expression var lvar
expression = S.Expression . hdoc

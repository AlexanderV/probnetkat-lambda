{- | 

Module:      ProbNetKat.Syntax.Parser
Description: Trifecta parser for ProbNetKAT programs
Maintainer:  alexander.vandenbroucke@kuleuven.be

Parser for the ProbNetKAT language.

Most operations (except application) are right-associative.
The priority of the operators is as follows (from tightest-binding to
loosest-binding):
  1. skip, drop, produce, force
  2. negation
  3. iteration
  4. application
  5. modification, test
  6. to
  7. choice
  8. sequential composition
  9. parallel composition
  10. if-then-else
  11. lambda-abstraction

Note: within predicates, the conjunction binds looser than negation,
and disjunction looser than conjunction.

-}

{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveFunctor #-}

module ProbNetKat.Syntax.Parser (
  NetkatParsing,
  -- * Predicate Parsing
  -- $predicate
  Predicate,
  predicate,
  -- * Value parsing
  -- $value
  Value,
  value, thunk, label, variable,
  -- * Expression Parsing
  -- $expression
  Expression,
  expression,
  -- * Program Parsing
  -- $program
  Program,
  program,
  toplevel,
  -- * Comment Parsing
  Uncommented(..), runUncommented
)
where


import           Control.Applicative (Alternative((<|>)))
import           Data.Char (isSpace)
import qualified Data.HashSet as HashSet
import           Data.Maybe (catMaybes)
import           Prelude hiding (drop,filter)
import qualified Text.Parser.Char as TriF
import           Text.Parser.Combinators ((<?>))
import qualified Text.Parser.Combinators as TriF
import qualified Text.Parser.Token as TriF
import qualified Text.Parser.Token.Highlight as TriF
import qualified Text.Trifecta.Parser as TriF

import           Control.Lens (set)

import qualified ProbNetKat.Syntax.Simple as Syntax


-------------------------------------------------------------------------------
-- Trifecta Parser style for ProbNetKAT

-- | Convenient synonym for parsing constraints.
type NetkatParsing m = (Monad m, TriF.Parsing m, TriF.TokenParsing m)

-- | NetKAT parsing style for identifiers.
netkatStyle :: (TriF.TokenParsing m) => TriF.IdentifierStyle m
netkatStyle = TriF.IdentifierStyle {
  TriF._styleName              = "identifier",
  TriF._styleStart             = TriF.lower,
  TriF._styleLetter            = TriF.alphaNum,
  TriF._styleReserved          = netkatReserved,
  TriF._styleHighlight         = TriF.Identifier,
  TriF._styleReservedHighlight = TriF.ReservedIdentifier
  }

-- | Parsing style for operators.
operatorStyle :: (TriF.TokenParsing m, Monad m) => TriF.IdentifierStyle m
operatorStyle =
  set TriF.styleReservedHighlight TriF.ReservedOperator netkatStyle

-- | Set of reserved ProbNetKAT keywords
netkatReserved :: HashSet.HashSet String
netkatReserved =
  HashSet.fromList [ "drop", "skip", "==", "&", ";", "~",
                     "<-", "dup", "+", ";", "<", ">", "*",
                     "if", "then", "else", "=", "\\", ".",
                     "force", "produce", "unit", "thunk", "to" ]

-------------------------------------------------------------------------------
-- Predicate parsing


{- $predicate
The concrete predicate syntax is described by the following grammar:

> P --> drop | skip | I == N | I == I | ~P | (P)

where I are identifiers (any alphabetic character followed by any string of
alphanumeric characters) and N are natural numbers.
-}

-- | A synonym for @'Syntax.Predicate' 'Value'@.
type Predicate = Syntax.Predicate Value

-- | Parse a predicate.
predicate :: NetkatParsing m => m Predicate
predicate = predicate9 <?> "predicate"

predicate9 :: NetkatParsing m => m Predicate
predicate9 = do
  p1 <- predicate8
  TriF.option p1 $ do
    TriF.reserve operatorStyle "&"
    p2 <- predicate9
    return (Syntax.Disjunction p1 p2)

predicate8 :: NetkatParsing m => m Predicate
predicate8 = do
  p1 <- predicate7
  TriF.option p1 $ do
    TriF.reserve operatorStyle ";"
    p2 <- predicate8
    return (Syntax.Conjunction p1 p2)
  
predicate7 :: NetkatParsing m => m Predicate
predicate7 = test <|> predicate4

predicate4 :: NetkatParsing m => m Predicate
predicate4 = negation <|> predicate0

predicate0 :: NetkatParsing m => m Predicate
predicate0 = drop <|> skip <|> TriF.parens predicate

test :: NetkatParsing m => m Predicate
test = (<?> "a test") $ do
  field <- TriF.try (value <* TriF.reserve operatorStyle "==")
  val <- value
  return (Syntax.Test field val)
  
drop :: NetkatParsing m => m Predicate
drop = TriF.reserve netkatStyle "drop" >> return Syntax.Drop
       <?> "drop"

skip :: NetkatParsing m => m Predicate
skip = TriF.reserve netkatStyle "skip" >> return Syntax.Skip
       <?> "skip"

negation :: (Monad m, TriF.TokenParsing m) => m Predicate
negation = negation' operatorStyle where
  negation'
    :: (Monad m, TriF.TokenParsing m)
    => (TriF.IdentifierStyle m) -> m Predicate
  negation' style = do
    _ <- TriF.highlight (TriF._styleReservedHighlight style) (TriF.string "~")
    p <- predicate4
    return (Syntax.Negation p)

-- | A synonym for @'Syntax.Value' String String@.
type Value = Syntax.Value String String


-------------------------------------------------------------------------------
-- Value Parsing

{- $value
The concrete value syntax is described by the following grammar:

> V --> unit | thunk(E) | F | N | I

where F are field labels (any uppercase alphabetica character followed by any
string of alphanumeric characters), I are identifiers (any lowercase alphabetic
character followed by any string of alphanumeric characters) and N are
natural numbers.
-}

-- | Parse a value.
value :: (Monad m, TriF.TokenParsing m) => m Value
value =
  (Syntax.unit <$ TriF.reserve netkatStyle "unit" <?> "unit")
  <|>
  thunk
  <|>
  label
  <|>
  (Syntax.literal <$> TriF.natural <?> "literal")
  <|>
  variable
  <|>
  TriF.parens value

-- | Parse a thunk.
thunk :: (Monad m, TriF.TokenParsing m) => m Value
thunk = (<?> "thunk") $ do
  TriF.reserve netkatStyle "thunk"
  Syntax.thunk <$> TriF.parens expression

-- | Parse a field label.
label :: (Monad m, TriF.TokenParsing m) => m Value
label = fmap Syntax.label $ TriF.token $ TriF.try $
  (:) <$> TriF.upper <*> TriF.many TriF.alphaNum <?> "field label"

-- | Parse a variable.
variable :: (Monad m, TriF.TokenParsing m) => m Value
variable = Syntax.variable <$> TriF.ident netkatStyle <?> "variable"

-------------------------------------------------------------------------------
-- Expression Parsing

{- $expression
The concrete expression syntax is described by the following grammar:

> E --> | P | I <- N | (E) | E* | E V | E <R> E | E; E | E & E
>     | if E then E else E | \I.E | force(V) | produce(V) | E to I.E

where I are identifiers (any lowercase alphabetic character followed by any
string of alphanumeric characters) and N are natural numbers.
Note that the grammar as presented here is left-recursive and ambiguous,
but becomes unambiguous when considering the operators' left-associativity
and precedence.
-}

-- Note: expressions only accept sub-expressions of a higher or equal priority.
-- Therefore, expression parsing is stratified along order of precedence:
-- iteration, application, to, choice, sequential composition, parallel
-- composition, if-then-else and lambdas in that order.
-- 
-- Furthermore, to avoid left-recursion every stratum has two production
-- symbols: a main production and a continuation (e.g. iteration and
-- iteration')

-- | A synonym for @'Syntax.Expression' String String@.
type Expression = Syntax.Expression String String

-- | Parse an expression.
expression :: NetkatParsing m => m Expression
expression = expression10 <?> "any expression"

expression10 :: NetkatParsing m => m Expression
expression10 = (expression10' <|>) $ (<?> "lambda") $ do
  _ <- TriF.char '\\'
  identifier <- TriF.ident netkatStyle <?> "identifier"
  _ <- TriF.char '.'
  TriF.whiteSpace
  expr <- expression
  return (Syntax.lambda identifier expr)

expression10' :: NetkatParsing m => m Expression
expression10' = (expression9 <|>) $ (<?> "if-then-else") $ do
  TriF.reserve netkatStyle "if"
  condition <- predicate
  TriF.reserve netkatStyle "then"
  prog1 <- expression
  TriF.reserve netkatStyle "else"
  prog2 <- expression
  return (Syntax.ifthenelse condition prog1 prog2)

expression9 :: NetkatParsing m => m Expression
expression9 = do
  e1 <- expression8
  TriF.option e1 $ do
    TriF.reserve operatorStyle "&"
    e2 <- expression9
    return (Syntax.parallel e1 e2)

expression8 :: NetkatParsing m => m Expression
expression8 = do
  e1 <- expression7
  TriF.option e1 $ do
    TriF.reserve operatorStyle ";"
    e2 <- expression8
    return (Syntax.sequential e1 e2)

expression7 :: NetkatParsing m => m Expression
expression7 = do
  e1 <- expression6
  TriF.option e1 $ do
    probability <- TriF.angles TriF.double
    e2 <- expression7
    return (Syntax.choice e1 probability e2)

expression6 :: NetkatParsing m => m Expression
expression6 = do
    e1 <- expression5
    TriF.option e1 $ do
      TriF.reserve netkatStyle "to"
      var <- TriF.ident netkatStyle <?> "identifier"
      _ <- TriF.char '.'
      TriF.whiteSpace
      expr2 <- expression6
      return (Syntax.to e1 var expr2)

expression5 :: NetkatParsing m => m Expression
expression5 =
  -- hack to not require parentheses around test and modification all the time
  modification <|>
  TriF.try (Syntax.filter <$> test) <|> do
    e <- expression4
    expression5' e where
      expression5' app = TriF.option app $ do
        v <- value
        expression5' (Syntax.application app v)

expression4 :: NetkatParsing m => m Expression
expression4 = do
  e <- expression0 
  expression4' e where
    expression4' e = TriF.option e $ do
      TriF.reserve operatorStyle "*"
      expression4' (Syntax.iteration e)

expression0 :: NetkatParsing m => m Expression
expression0 =
  duplication
  <|> produce
  <|> force
  <|> TriF.try (Syntax.filter <$> predicate4)
  <|> TriF.parens expression
  -- The following is a hack to support top-level computation variables.
  -- Variables are only supported in value positions, hence
  -- We must create such a value position inside a force.
  <|> fmap Syntax.force variable

modification :: (Monad m, TriF.TokenParsing m) => m Expression
modification = (<?> "modification") $ do
  field <- TriF.try (value <* TriF.reserve operatorStyle "<-")
  val <- value
  return (Syntax.modification field val)

duplication :: NetkatParsing m => m Expression
duplication = (<?> "duplication") $ do
  TriF.reserve netkatStyle "dup"
  return Syntax.duplication

force :: NetkatParsing m => m Expression
force = (<?> "force") $ do
  TriF.reserve netkatStyle "force"
  Syntax.force <$> TriF.parens value

produce :: NetkatParsing m => m Expression
produce = (<?> "produce") $ do
  TriF.reserve netkatStyle "produce"
  Syntax.produce <$> TriF.parens value


-------------------------------------------------------------------------------
-- Program parsing

{- $program
Programs consist of several top-level definitions, separated by newlines,
and a single main expression. It's concrete syntax is described by:

> P --> I = E<newline>P | E

where I are identifiers (any alphabetic character followed by any string of
alphanumeric characters).
-}

-- | A type synonym for @'Syntax.Program' String String@
type Program = Syntax.Program String String

-- | Parse a program.
program :: TriF.Parser Program
program = (<?> "program") $ runUncommented $ do
  TriF.whiteSpace
  toplevels <- toplevel' `TriF.endBy` TriF.token interNewline
  main_expression <- expression <?> "main expression"
   -- parse remaining newlines
  TriF.skipMany (TriF.token TriF.newline)
  TriF.eof
  return (Syntax.program (catMaybes toplevels) main_expression)

-- | Parse a top-level assignment or a blank line
toplevel' :: NetkatParsing m => m (Maybe (String, Expression))
toplevel' = fmap Just toplevel <|> return Nothing

-- | Parse a top-level assignment.
toplevel :: NetkatParsing m => m (String, Expression)
toplevel = (<?> "top level assignment") $ do
  identifier <- TriF.ident netkatStyle
  TriF.reserve operatorStyle "="
  expr <- expression
  TriF.whiteSpace
  return (identifier, expr)

-------------------------------------------------------------------------------
-- Comment parsing

-- | A parser transformer that treats comments as whitespace, but does not
-- automatically consume newlines.
newtype Uncommented m a = Uncommented {
  _runUncommented :: m a
} deriving (Functor,Applicative,Monad,Alternative,TriF.Parsing,TriF.CharParsing)

-- | Run an 'Uncommented' parser.
runUncommented :: Uncommented m a -> m a
runUncommented = _runUncommented

instance TriF.TokenParsing m => TriF.TokenParsing (Uncommented m) where
  someSpace = Uncommented $
    TriF.skipSome $
      comment <|> TriF.try intraNewline <|> someSpace'
  nesting (Uncommented m) = Uncommented (TriF.nesting m)
  semi = Uncommented TriF.semi
  highlight h (Uncommented m) = Uncommented (TriF.highlight h m)

-- | A newline within a top-level definition
intraNewline :: TriF.TokenParsing m => m ()
intraNewline = TriF.skipSome TriF.newline *> runUncommented TriF.someSpace

-- | A newline within a top-level definition
interNewline :: TriF.TokenParsing m => m ()
interNewline = TriF.newline *> TriF.notFollowedBy someSpace'

-- | Skip a comment
comment :: TriF.TokenParsing m => m ()
comment =
  TriF.string "//" *> TriF.skipMany (TriF.satisfy (/= '\n'))
  <?> "comment"
  
-- | Workaround for the incorrect someSpace implementation of 'Unlined'.
-- Parses at least one whitespace character that is not a newline.
someSpace' :: TriF.TokenParsing m => m ()
someSpace' =
  TriF.satisfy (\c -> c /= '\n' && isSpace c) *> TriF.whiteSpace <?> "whitespace"

{- |

Module:      ProbNetKat.Syntax.Simple
Description: Syntax tree datatypes for Simple ProbNetKAT expressions
Maintainer:  alexander.vandenbroucke@kuleuven.be

Simple expressions are expressions that don't contain any extensions, and
where the expression extension constructor 'Expression' is closed.

-}

{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module ProbNetKat.Syntax.Simple (
  -- * Simple Expressions
  Predicate(..),
  Simple,
  XUnit(XU),
  XClosed,
  absurdX,
  Value,
  Expression,
  Program,
  -- ** Smart Constructors for simple values
  unit, thunk, label, literal, variable,
  -- ** Smart constructors for simple expressions
  duplication, ProbNetKat.Syntax.Simple.filter, modification, parallel,
  sequential, choice, iteration, lambda, application, force, produce, to,
  ifthenelse,
  -- ** Useful functions
  simplifyV,
  simplify,
  program,
  collapsed
)

where

import           Control.DeepSeq (NFData)
import           Data.Bifunctor (Bifunctor(bimap))
import           Data.Void
import           GHC.Generics (Generic)
import qualified ProbNetKat.Syntax as S
import           ProbNetKat.Syntax as S hiding (Value,Expression,Program,XUnit)
import           ProbNetKat.Syntax.Unparse (Unparse)
import qualified ProbNetKat.Syntax.Unparse as U
import           Text.Pretty

-------------------------------------------------------------------------------
-- Simple expressions

-- | Simple expression type
data Simple

-- | Extension that doesn't carry any information
data XUnit var lvar = XU deriving (Show,Eq,Ord,Generic)

instance NFData (XUnit var lvar)

instance Bifunctor XUnit where
  bimap _ _ XU = XU

-- | Extension that is closed
newtype XClosed var lvar = XClosed Void deriving (Eq,Show,Generic,NFData)

instance Bifunctor XClosed where
  bimap _ _ = absurdX

-- | Pattern matching on XClosed is absurd
absurdX :: XClosed var lvar  -> a
absurdX (XClosed void) = absurd void

type instance S.XUnit Simple = XUnit
type instance XThunk Simple = XUnit
type instance XLabel Simple = XUnit
type instance XLiteral Simple = XUnit
type instance XVariable Simple = XUnit

type instance XDuplication Simple = XUnit
type instance XFilter Simple      = XUnit
type instance XModification Simple = XUnit
type instance XParallel Simple = XUnit
type instance XSequential Simple = XUnit
type instance XChoice Simple = XUnit
type instance XIteration Simple = XUnit
type instance XLambda Simple   = XUnit
type instance XForce Simple = XUnit
type instance XProduce Simple = XUnit
type instance XTo Simple = XUnit
type instance XApplication Simple = XUnit
type instance XExpression Simple = XClosed

-- | Values that aren't anotated with anything.
type Value = S.Value Simple

-- | Expressions that aren't anotated with anything.
type Expression = S.Expression Simple

-- | Programs that aren't anotated with anything.
type Program = S.Program Simple

instance (Pretty var, Pretty lvar) => Pretty (Value var lvar) where
  prettyPrec p val = U.docOfV (U.unparseV val) p

instance (Pretty var, Pretty lvar) => Pretty (Expression var lvar) where
  prettyPrec p expr = U.docOf (U.unparse expr) p

instance (Pretty var, Pretty lvar) => Unparse Simple var lvar where
  unparseV =
    (xfoldV
     (const U.unit)
     (const (U.thunk . U.unparse))
     (const U.label)
     (const U.literal)
     (const U.variable))
  unparse =    
    (xfold
     (const U.duplication)
     (const (U.filter . U.unparseP))
     (const $ \f v -> U.modification (U.unparseV f) (U.unparseV v))
     (const U.parallel)
     (const U.sequential)
     (const U.choice)
     (const U.iteration)
     (const U.lambda)
     (const $ \e -> U.application e . U.unparseV)
     (const $ U.force . U.unparseV)
     (const $ U.produce . U.unparseV)
     (const   U.to)
     absurdX)

instance (Pretty var, Pretty lvar) => Pretty (Program var lvar) where
  pretty = prettyProgram

-- | Simple unit
unit :: Value var lvar
unit = Unit XU

-- | Simple thunk
thunk :: Expression var lvar -> Value var lvar
thunk = Thunk XU

-- | Simple label
label :: String -> Value var lvar
label = Label XU

-- | Simple literal
literal :: Integer -> Value var lvar
literal = Literal XU

-- | Simple variable
variable :: var -> Value var lvar
variable = Variable XU

-- | Simple duplication
duplication :: Expression var lvar
duplication = Duplication XU

-- | Simple filter
filter :: Predicate (Value var lvar) -> Expression var lvar
filter = Filter XU

-- | Simple modification
modification :: Value var lvar -> Value var lvar -> Expression var lvar
modification = Modification XU

-- | Simple parallel composition
parallel
  :: Expression var lvar
  -> Expression var lvar
  -> Expression var lvar
parallel = Parallel XU

-- | Simple sequential composition
sequential
  :: Expression var lvar
  -> Expression var lvar
  -> Expression var lvar
sequential = Sequential XU

-- | Simple probabilistic choice
choice
  :: Expression var lvar
  -> Double
  -> Expression var lvar
  -> Expression var lvar
choice = Choice XU

-- | Simple iteration
iteration :: Expression var lvar -> Expression var lvar
iteration = Iteration XU

-- | Simple lambda abstraction.
lambda :: lvar -> Expression var lvar -> Expression var lvar
lambda = Lambda XU

-- | Simple function application
application
  :: Expression var lvar
  -> Value var lvar
  -> Expression var lvar
application = Application XU

-- | Simple force
force :: Value var lvar -> Expression var lvar
force = S.Force XU

-- | Simple produce
produce :: Value var lvar -> Expression var lvar
produce = S.Produce XU

-- | Simple to
to :: Expression var lvar -> lvar -> Expression var lvar -> Expression var lvar
to = S.To XU

-- | Simple if then else
ifthenelse
  :: Predicate (Value var lvar)
  -> Expression var lvar
  -> Expression var lvar
  -> Expression var lvar
ifthenelse = xifthenelse XU XU XU

-- | Collapse a 'Program' into a single 'Expression'.
-- The toplevel definitions are substituted into the main expression.
-- Note: If the top-level definitions are cyclic, this function may not
-- terminate.
collapsed :: Eq var => Program var var -> Expression var var
collapsed = xcollapsed thunk

-- | A simple program
program
  :: [(lvar, Expression var lvar)] -> Expression var lvar -> Program var lvar
program = MkProgram


-- | Turn an extende @'Value' ext@ into a simple value.
simplifyV
  :: (XExpression ext var lvar -> Expression var lvar)
  -> S.Value ext var lvar -> Value var lvar
simplifyV simplifyXExpression =
  (xfoldV
   (const unit)
   (const (thunk . simplify simplifyXExpression))
   (const label)
   (const literal)
   (const variable))

-- | Turn an extended @'Expression' ext@ into a simple expression.
simplify
  :: (XExpression ext var lvar -> Expression var lvar)
  -> S.Expression ext var lvar -> Expression var lvar
simplify simplifyXExpression =
  let simplifyV' = simplifyV simplifyXExpression
  in (xfold
      (const duplication)
      (const $
        ProbNetKat.Syntax.Simple.filter . fmap simplifyV')
      (const $ \f n -> modification (simplifyV' f) (simplifyV' n))
      (const parallel)
      (const sequential)
      (const choice)
      (const iteration)
      (const lambda)
      (const $ \e v -> application e (simplifyV' v))
      (const $ force . simplifyV')
      (const $ produce . simplifyV')
      (const to)
      simplifyXExpression)

{- |

Module:      ProbNetKat.Syntax.DeBruijn
Description: Syntax tree datatypes for DeBruijn-indexed ProbNetKAT expressions
Maintainer:  alexander.vandenbroucke@kuleuven.be

The extension field for variables carries the variable's DeBruijn index (the
distance to its binder). All other extension fields are unit and the
extension constructor is closed.

-}


{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}

module ProbNetKat.Syntax.DeBruijn (
  -- * DeBruijn indexed syntax
  DeBruijn, HIndex(..),
  Value, Expression,
  -- ** Smart constructors
  unit, thunk, label, literal, variable, duplication,
  ProbNetKat.Syntax.DeBruijn.filter, modification, parallel, sequential,
  choice, iteration, lambda, force, produce, application, to,

  -- * Shifting and Substitution
  protect, shiftV, shiftE, Substitute(substitute),

  -- * Turning 'Simple.Simple' syntax into 'DeBruijn'
  addDeBruijnV, addDeBruijn
)
where


import           ProbNetKat.Syntax hiding (XUnit,Value,Expression)
import qualified ProbNetKat.Syntax as S
import           ProbNetKat.Syntax.Simple (XUnit(XU),XClosed)
import qualified ProbNetKat.Syntax.Simple as Simple
import qualified ProbNetKat.Syntax.Unparse as U


import           Data.Maybe (fromMaybe)
import           Data.List (elemIndex)
import           Data.Semigroup ((<>))
import           Text.Pretty (Pretty(pretty,prettyPrec),parens)
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import           Text.PrettyPrint.ANSI.Leijen ((</>))


-------------------------------------------------------------------------------
-- De Bruijn Indexed expressions

-- | DeBruijn expression type
data DeBruijn

-- | Higher-order wrapper around a de Bruijn index
newtype HIndex var lvar = HIndex { unHIndex :: Int } deriving (Show,Pretty,Eq)

type instance S.XUnit DeBruijn = XUnit
type instance XThunk DeBruijn = XUnit
type instance XLabel DeBruijn = XUnit
type instance XLiteral DeBruijn = XUnit
type instance XVariable DeBruijn = HIndex

type instance XDuplication DeBruijn = XUnit
type instance XFilter DeBruijn      = XUnit
type instance XModification DeBruijn = XUnit
type instance XParallel DeBruijn = XUnit
type instance XSequential DeBruijn = XUnit
type instance XChoice DeBruijn = XUnit
type instance XIteration DeBruijn = XUnit
type instance XLambda DeBruijn   = XUnit
type instance XForce DeBruijn = XUnit
type instance XProduce DeBruijn = XUnit
type instance XTo DeBruijn = XUnit
type instance XApplication DeBruijn = XUnit
type instance XExpression DeBruijn = XClosed

-- | Values that are annotated with de Bruijn indices
type Value = S.Value DeBruijn

-- | Expressions that are annotated with de Bruijn indices
type Expression = S.Expression DeBruijn

instance Pretty (Expression var lvar) where
  prettyPrec p expr = U.docOf (U.unparse expr) p

instance U.Unparse DeBruijn var lvar where
  unparseV = 
    (S.xfoldV
     (const   U.unit)
     (const $ U.thunk . U.unparse)
     (const   U.label)
     (const   U.literal)
     (S.Variable . U.hdoc . pretty . unHIndex))
  unparse =
    (S.xfold
     (const U.duplication)
     (const $ U.filter . fmap U.unparseV)
     (const $ \f v -> U.modification (U.unparseV f) (U.unparseV v))
     (const   U.parallel)
     (const   U.sequential)
     (const   U.choice)
     (const   U.iteration)
     (const   unparseLambda)
     (const $ \e v -> U.application e (U.unparseV v))
     (const $ U.force . U.unparseV)
     (const $ U.produce . U.unparseV)
     (const   unparseTo)
     Simple.absurdX)

unparseLambda :: lvar -> U.Expression var lvar -> U.Expression var lvar
unparseLambda var expr =
  let docp p = parens (p < 10) $
        PP.backslash <> PP.dot PP.<//> U.docOf expr 10 
  in S.Lambda (U.HDoc docp) var expr

unparseTo
  :: U.Expression var lvar
  -> lvar
  -> U.Expression var lvar
  -> U.Expression var lvar
unparseTo e1 var e2 =
  let docp p = parens (p < 5) $
        U.docOf e1 10 </> PP.string "to" <> PP.dot </> U.docOf e2 10
  in S.To (U.HDoc docp) e1 var e2


-------------------------------------------------------------------------------
-- Smart constructors

-- | DeBruijn-indexed unit
unit :: Value var lvar
unit = Unit XU

-- | DeBruijn-indexed thunk
thunk :: Expression var lvar -> Value var lvar
thunk = Thunk XU

-- | DeBruijn-indexed label
label :: String -> Value var lvar
label = Label XU

-- | DeBruijn-indexed literal
literal :: Integer -> Value var lvar
literal = Literal XU

-- | DeBruijn-indexed variable. The first argument is the index (distance to
-- the binder of this variable).
variable :: Int -> var -> Value var lvar
variable i = Variable (HIndex i)

-- | DeBruijn-indexed duplication
duplication :: Expression var lvar
duplication = Duplication XU

-- | DeBruijn-indexed filter
filter :: Predicate (Value var lvar) -> Expression var lvar
filter = Filter XU

-- | DeBruijn-indexed modification
modification :: Value var lvar -> Value var lvar -> Expression var lvar
modification = Modification XU

-- | DeBruijn-indexed parallel composition
parallel
  :: Expression var lvar
  -> Expression var lvar
  -> Expression var lvar
parallel = Parallel XU

-- | DeBruijn-indexed sequential composition
sequential
  :: Expression var lvar
  -> Expression var lvar
  -> Expression var lvar
sequential = Sequential XU

-- | DeBruijn-indexed choice
choice
  :: Expression var lvar
  -> Double
  -> Expression var lvar
  -> Expression var lvar
choice = Choice XU

-- | DeBruijn-indexed iteration
iteration :: Expression var lvar -> Expression var lvar
iteration = Iteration XU

-- | DeBruijn-indexed lambda abstraction
lambda :: lvar -> Expression var lvar -> Expression var lvar
lambda = Lambda XU

-- | DeBruijn-indexed application
application
  :: Expression var lvar
  -> Value var lvar
  -> Expression var lvar
application = Application XU

-- | DeBruijn-indexed force
force :: Value var lvar -> Expression var lvar
force = S.Force XU

-- | DeBruijn-indexed produce
produce :: Value var lvar -> Expression var lvar
produce = S.Produce XU

-- | DeBruijn-indexed to
to :: Expression var lvar -> lvar -> Expression var lvar -> Expression var lvar
to = S.To XU

-------------------------------------------------------------------------------
-- Substitutions on deBruijn-indexed variables

-- | Substitute variables inside a deBruijn-indexed value.
instance Substitute Int (Value var lvar) (Value var lvar) where
  substitute env v@(Variable (HIndex k) _) = fromMaybe v (env k)
  substitute env (Thunk x e) = Thunk x (substitute env e)
  substitute _ v = v

-- | Substitute variables inside a DeBruijn expression, ignore extensions.
instance Substitute Int (Value var lvar) (Expression var lvar) where
  substitute env = go where
    go e@Duplication{} = e
    go (Filter XU p) = Filter XU (substitute env p)
    go (Modification XU v1 v2) =
      modification (substitute env v1) (substitute env v2)
    go (Parallel XU e1 e2) = parallel (go e1) (go e2)
    go (Sequential XU e1 e2) = sequential (go e1) (go e2)
    go (Choice XU e1 p e2) = choice (go e1) p (go e2)
    go (Iteration XU e) = iteration (go e)
    go (Lambda XU var e) = lambda var (substitute (protect env) e)
    go (Application XU e v) = application (go e) (substitute env v)
    go (Force XU v) = force (substitute env v)
    go (Produce XU v) = produce (substitute env v)
    go (To XU e1 var e2) =
      to (substitute env e1) var (substitute (protect env) e2)
    go e@S.Expression{} = e

-- | Protect the variables in the environment.
protect :: (Int -> Maybe (Value var lvar)) -> (Int -> Maybe (Value var lvar))
protect env' x = shiftV 0 1 <$> env' (x - 1)

-- | Shift a value by a given distance.
-- @shiftV cutoff dist@ shifts all variables' de Bruijn indices that are not
-- less than @cutoff@ by @dist@.
shiftV :: Int -> Int -> Value var lvar -> Value var lvar
shiftV c d v = case v of
  Thunk XU e -> thunk (shiftE c d e)
  Variable (HIndex k) var | k >= c -> variable (k + d) var
    -- only shift variables if we're above the cutoff
  _ -> v

-- | Shift an expression by a given distance.
-- @shiftE cutoff dist@ shifts all variables' de Bruijn indices that are not
-- less than @cutoff@ by @dist@.
shiftE :: Int -> Int -> Expression var lvar -> Expression var lvar
shiftE c d = go where
  go e@Duplication{} = e
  go (Filter x p) = Filter x (fmap (shiftV c d) p)
  go (Modification XU v1 v2) = modification (shiftV c d v1) (shiftV c d v2)
  go (Parallel XU e1 e2) = parallel (go e1) (go e2)
  go (Sequential XU e1 e2) = sequential (go e1) (go e2)
  go (Choice XU e1 p e2) = choice (go e1) p (go e2)
  go (Iteration XU e) = iteration (go e)
  go (Lambda XU var e) = lambda var (shiftE (c + 1) d e)
  go (Application XU e v) = application (go e) (shiftV c d v)
  go (Force XU v) = force (shiftV c d v)
  go (Produce XU v) = produce (shiftV c d v)
  go (To XU e1 var e2) = to (go e1) var (shiftE (c + 1) d e2)
  go e@S.Expression{} = e

-- | Look up a variable in a naming context.
getIndex :: Eq var => [var] -> var -> Int
getIndex naming name = case elemIndex name naming of
  Nothing -> error "addDeBruijn: variable not found in naming context ."
  Just i  -> i

-- | Add de Bruijn indices to simple values.
-- The function takes a naming context and a simple value and annotates all
-- variables with their de Bruijn indices.
addDeBruijnV :: (Eq var) => [var] -> Simple.Value var var -> Value var var
addDeBruijnV naming = go where
  go (Variable XU name) = variable (getIndex naming name) name
  go (Thunk XU e) = thunk (addDeBruijn naming e)
  go (Unit XU) = unit
  go (Label XU l) = label l
  go (Literal XU i) = literal i

-- | Add de Bruijn indices to a simple expression.
-- The function takes a naming context and a simple expression and annotates
-- all variables with their de Bruijn indices.
addDeBruijn
  :: (Eq var)
  => [var] -> Simple.Expression var var -> Expression var var
addDeBruijn naming = go where
  go Duplication{} = Duplication XU
  go (Filter XU p) = Filter XU (fmap (addDeBruijnV naming) p)
  go (Modification XU v1 v2) =
    Modification XU (addDeBruijnV naming v1) (addDeBruijnV naming v2)
  go (Parallel XU e1 e2) = Parallel XU (go e1) (go e2)
  go (Sequential XU e1 e2) = Sequential XU (go e1) (go e2)
  go (Choice XU e1 p e2) = Choice XU (go e1) p (go e2)
  go (Iteration XU e) = Iteration XU (go e)
  go (Lambda XU var e) = Lambda XU var (addDeBruijn (var:naming) e)
  go (Application XU e v) = Application XU (go e) (addDeBruijnV naming v)
  go (Force XU v) = Force XU (addDeBruijnV naming v)
  go (Produce XU v) = Produce XU (addDeBruijnV naming v)
  go (To XU e1 var e2) = To XU (go e1) var (addDeBruijn (var:naming) e2)
  go (S.Expression x) = Simple.absurdX x

{- | 

Module:      ProbNetKat.Semantics
Description: Approximation to Scott semantics for ProbNetKAT
Maintainer:  alexander.vandenbroucke@kuleuven.be

Approximation to Scott semantics for ProbNetKAT.

See the "Cantor meets Scott" paper for details.
-}

{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE BangPatterns #-}

module ProbNetKat.Semantics (
  -- * Packets and Packet Histories
  Packet, PacketHistory(MkPH),
  test, isDefined, modify, duplicate, blank, historyLength, toList, head,
  -- * Histories
  Log(..), fromDouble, toDouble, invert,
  Histories(..),
  dirac, scalarHistories, addHistories, cartProd, bind, integrate,
  tabulateHistories,
  -- * Semantics
  Predicate,
  Expression,
  Program,
  Value,
  evalP, evalE,
  runN, runE, run,
  inline)
where

import           Control.DeepSeq
import           Control.Monad ((>=>))
import           Data.Bifunctor (second)
import           Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import           Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import           Data.Hashable
import           GHC.Generics (Generic)
import           Prelude hiding (head)
import           Text.Pretty
import qualified Text.PrettyPrint.ANSI.Leijen as PP


import           ProbNetKat.Result
import qualified ProbNetKat.Semantics.Compilation as C
import qualified ProbNetKat.Syntax as S
import qualified ProbNetKat.Syntax.DeBruijn as DB
import qualified ProbNetKat.Syntax.Simple as Simple

-------------------------------------------------------------------------------
-- Packets and Packet histories

-- | Packets map fields (identified by strings) to integers.
type Packet = HashMap String Integer

-- | Packet histories are non-empty lists of packets
data PacketHistory = MkPH Packet [Packet] deriving (Eq, Generic)

instance Hashable PacketHistory where
instance NFData PacketHistory where

-- | Test if a field of the head packet of a history has a certain value.
test :: String -> Integer -> PacketHistory -> Bool
test field value (MkPH packet _) = HashMap.lookup field packet == Just value
{-# INLINEABLE test #-}

-- | Test if a field of the head packet of a history is defined.
isDefined :: String -> PacketHistory -> Bool
isDefined field (MkPH packet _) = HashMap.member field packet
{-# INLINEABLE isDefined #-}

-- | Change the value of a field of the head packet of a history.
modify :: String -> Integer -> PacketHistory -> PacketHistory
modify field value (MkPH packet ps) =
  MkPH (HashMap.insert field value packet) ps
{-# INLINEABLE modify #-}

-- | Duplicate the head packet of a history.
duplicate :: PacketHistory -> PacketHistory
duplicate (MkPH packet ps) = MkPH packet (packet:ps)
{-# INLINEABLE duplicate #-}

-- | Convert a history into a list.
toList :: PacketHistory -> [Packet]
toList (MkPH p ps) = p:ps
{-# INLINEABLE toList #-}

-- | Length of a history.
historyLength :: PacketHistory -> Int
historyLength (MkPH _ ps) = length ps + 1
{-# INLINEABLE historyLength #-}

-- | A packet history containing a single blank packet (all fields unset).
blank :: HashSet PacketHistory
blank = HashSet.singleton (MkPH HashMap.empty [])
{-# INLINEABLE blank #-}

-- | The most recent packet in a 'PacketHistory'.
head :: PacketHistory -> Packet
head (MkPH packet _) = packet
{-# INLINEABLE head #-}

-- | Tabulate @HashSet PacketHistory@ nicely, given a list of headers.
tabulatePacketHistorySet :: [String] -> HashSet PacketHistory -> PP.Doc
tabulatePacketHistorySet headers set
  | HashSet.null set = PP.text "empty set"
  | otherwise = 
      let histories = HashSet.toList set
      in PP.vsep (map (tabulatePacketHistory headers) histories)

tabulatePacketHistory :: [String] -> PacketHistory -> PP.Doc
tabulatePacketHistory headers (MkPH packet packets) =
  PP.hsep [tabulatePacket headers p | p <- packet:packets]

tabulatePacket :: [String] -> Packet -> PP.Doc
tabulatePacket headers packet =
  let literal h = PP.fill (length h) $ case HashMap.lookup h packet of
        Nothing -> PP.empty
        Just i  -> PP.integer i
      literals = map literal headers
      bar = PP.text "|"
  in bar PP.<+> PP.hsep (PP.punctuate (PP.text "|") literals) PP.<+> bar


-------------------------------------------------------------------------------
-- Histories (a datastructure for weighted sets of packet histories).

-- | A datatype for log-probabilities.
--
-- This newtype represents a probability by its natural logarithm. This
-- is advantageous when multiplying many small probabilities.
newtype Log = MkLog {
  unLog :: Double
} deriving (Eq,Ord,Show,Hashable,NFData)

instance Pretty Log where
  pretty (MkLog l) = pretty (exp l * 100) PP.<> PP.text "%"

-- | Represent a given 'Double' as a 'Log'.
fromDouble :: Double -> Log
fromDouble = MkLog . log
{-# INLINEABLE fromDouble #-}

-- | Return the 'Double' corresponding to the 'Log'.
toDouble :: Log -> Double
toDouble = exp . unLog
{-# INLINEABLE toDouble #-}


-- | Given @log x@, this returns @log (1 - x)@.
invert :: Log -> Log
invert (MkLog y) = MkLog (log (1 - exp y))
{-# INLINEABLE invert #-}

instance Num Log where
  MkLog x + MkLog y = MkLog (log (exp x + exp y))
  {-# INLINEABLE (+) #-}
  MkLog x * MkLog y = MkLog (x + y)
  {-# INLINEABLE (*) #-}
  abs x = x
  signum _ = 1
  negate = error "negate: Negation is not allowed on Log."
  fromInteger = fromDouble . fromInteger

instance Fractional Log where
  fromRational = MkLog . log . fromRational
  {-# INLINEABLE fromRational #-}
  recip (MkLog x) = (MkLog (negate x))
  {-# INLINEABLE recip #-}

-- | A representation of a sub-probability distribution over sets of packet
-- histories.
newtype Histories = MkHistories {
  unHistories :: HashMap (HashSet PacketHistory) Log
} deriving Generic

instance NFData Histories

-- | Create 'Histories' with a single set of packet histories, with weight 1.
dirac :: HashSet PacketHistory -> Histories
dirac set = MkHistories (HashMap.singleton set 1.0)
{-# INLINEABLE dirac #-}

-- | Multiply all probabilities with a scalar value.
scalarHistories :: Log -> Histories -> Histories
scalarHistories prob (MkHistories !hists) =
  MkHistories (HashMap.map (prob *) hists)
{-# INLINEABLE scalarHistories #-}  

-- | Add the probabilities of two 'Histories'.
addHistories :: Histories -> Histories -> Histories
addHistories (MkHistories !hist1) (MkHistories !hist2) =
  MkHistories (HashMap.unionWith (+) hist1 hist2)
{-# INLINEABLE addHistories #-}    

-- | Take the cartesian product of two 'Histories'.
cartProd :: Histories -> Histories -> Histories
cartProd (MkHistories !hist1) (MkHistories !hist2) =
  let cartProd' hist a p =
        let cartProd'' hist' b q =
              HashMap.insertWith (+) (HashSet.union a b) (p * q) hist'
        in HashMap.foldlWithKey' cartProd'' hist hist2
  in MkHistories (HashMap.foldlWithKey' cartProd' HashMap.empty hist1)
{-# INLINEABLE cartProd #-}

-- | Apply a continuation to a History.
bind
  :: Monad m
  => Histories -> (HashSet PacketHistory -> m Histories) -> m Histories
bind (MkHistories !hist) k =
  let bind' hist' a p =
        let k' = fmap (unHistories . scalarHistories p) (k a)
        in HashMap.unionWith (+) <$> hist' <*> k'
  in fmap MkHistories (HashMap.foldlWithKey' bind' (return HashMap.empty) hist)
{-# INLINEABLE bind #-}

-- | Integrate a 'Log'-valued function with respect to @Histories@.
integrate :: Histories -> (HashSet PacketHistory -> Log) -> Double
integrate (MkHistories !hist) f =
  let f' total a p = toDouble (p * f a) + total
  in HashMap.foldlWithKey' f' 0 hist

-- | Tabulate @Histories@ nicely, given a list of headers
tabulateHistories :: [String] -> Histories -> PP.Doc
tabulateHistories headers (MkHistories !hist) =
  let bar = PP.text "|"
      headerline =
        bar PP.<+> PP.hsep (PP.punctuate bar (map PP.text headers)) PP.<+> bar
  in PP.vsep [
    PP.vsep [
        PP.text "set: " PP.<+> pretty p,
        headerline,
        tabulatePacketHistorySet headers phs]
    | (phs,p) <- HashMap.toList hist ]

-- instance Pretty Histories where
--   pretty (MkHistories hist) =  

-------------------------------------------------------------------------------
-- Semantics

-- | Convenient 'S.Predicate Value' synonym 
type Predicate = S.Predicate Value

-- | Convenient 'C.Value' synonym 
type Value = C.Value String String

-- | Convenient 'C.Expression' synonym 
type Expression = C.Expression String String

-- | Convenient 'Simple.Program' synonym 
type Program    = Simple.Program String String

-- | Evaluate a predicate with respect to a set of packet histories.
evalP
  :: MonadError PP.Doc m
  => Predicate -> HashSet PacketHistory -> m (HashSet PacketHistory)
evalP p !histories = case p of
  S.Drop ->
    return (HashSet.empty)
  S.Skip ->
    return histories
  S.Test (S.Label _ field) (S.Literal _ value) ->
    return (HashSet.filter (test field value) histories)
  S.Test{} -> throwError (PP.string "evalP: malformed test")
  S.Disjunction p1 p2 ->
     HashSet.union <$> evalP p1 histories <*> evalP p2 histories
  S.Conjunction p1 p2 ->
    evalP p1 histories >>= evalP p2
  S.Negation p' ->
    HashSet.difference histories <$> evalP p' histories
{-# INLINEABLE evalP #-}

    
-- | Evaluate @n@-th approximation of an expression with respect to a set of
-- histories.
evalE
  :: Int
  -> HashSet PacketHistory
  -> Expression  
  -> Result Histories
evalE n !histories expr = case expr of
  S.Filter _ predicate -> do
    set <- evalP predicate histories
    return $! dirac $! set
  S.Modification _ (S.Label _ field) (S.Literal _ value) ->
    return $! dirac $! HashSet.map (modify field value) histories
  S.Modification{} -> throwError (PP.string "evalE: malformed modification")
  S.Duplication _ ->
    return $! dirac $! HashSet.map duplicate histories
  S.Parallel _ prog1 prog2 -> {-# SCC "parallel" #-} do
    hist1 <- evalE n histories prog1
    hist2 <- evalE n histories prog2
    return $! cartProd hist1 hist2
  S.Sequential _ prog1 prog2 -> do
    hist1 <- evalE n histories prog1
    hist1 `bind` \phs -> evalE n phs prog2
  S.Choice _ prog1 prob prog2 -> do
    hist1 <- evalE n histories prog1
    hist2 <- evalE n histories prog2
    let p = fromDouble prob
    return $!
      scalarHistories p hist1 `addHistories` scalarHistories (invert p) hist2
  S.Iteration _ p -> {-# SCC "iteration" #-}
    let iterated i !hist
          | i <= 0    =
              return (dirac hist)
          | otherwise = do
              histn <- iterated (i-1) hist
              hist1 <- histn `bind` \h -> evalE n h p
              return $! cartProd (dirac hist) hist1
    in iterated n histories
  S.Lambda x _ _ -> Simple.absurdX x
  S.Application x _ _ -> Simple.absurdX x
  S.Force x _ -> Simple.absurdX x
  S.Produce x _ -> Simple.absurdX x
  S.To x _ _ _ -> Simple.absurdX x
  S.Expression x -> Simple.absurdX x

-- | Run 0-th approximation of an expression with respect to a set of histories.
run
  :: HashSet PacketHistory
  -> Program
  -> Result Histories
run ph = runN 0 ph

-- | Run n-th approximation of an expression with respect to a set of histories.
runE
  :: Int
  -> HashSet PacketHistory
  -> Simple.Expression String String
  -> Result Histories
runE n ph = C.erase . DB.addDeBruijn [] >=> evalE n ph

-- | Run n-th approximation of a program with respect to a set of histories.
runN
  :: Int
  -> HashSet PacketHistory
  -> Program
  -> Result Histories
runN n ph = C.compile . inline >=> evalE n ph

-- | Inline the top level definitions of a program.
inline :: Simple.Program String String -> DB.Expression String String
inline (S.MkProgram toplevels main) =
  let naming = map fst toplevels
      toplevels' = map (second $ DB.addDeBruijn naming) toplevels
      toplevels'' = map (subst . snd) toplevels'
      subst = S.substitute $ \var ->
        if 0 <= var && var < length toplevels then
          Just $ DB.thunk (toplevels'' !! var)
        else
          Nothing
  in subst (DB.addDeBruijn naming main)

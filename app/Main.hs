{-# LANGUAGE OverloadedStrings #-}

module Main where

import           System.Environment (getProgName, getArgs)
import           Text.Trifecta.Parser (parseFromFileEx)
import qualified Text.Trifecta.Result as TriF

import           ProbNetKat.Result
import qualified ProbNetKat.Semantics as Semantics
import qualified ProbNetKat.Semantics.Compilation as C
import qualified ProbNetKat.Semantics.Compilation.Typing as CT
import qualified ProbNetKat.Syntax.Parser as P
import qualified ProbNetKat.Syntax.Simple as S
import qualified ProbNetKat.Typing.Reconstruction as R
import           System.FilePath ((-<.>))
import           System.IO (stderr,withFile,IOMode(WriteMode))
import           Text.Pretty (pretty,putDocLn,hPutDoc,hPutDocLn)
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import           Text.PrettyPrint.ANSI.Leijen as PP ((</>))

main :: IO ()
main = do
  me <- getProgName
  filenames <- getArgs
  case filenames of
    [] -> putDocLn (PP.string me <> PP.colon </> "no input files")
    _ -> mapM_ compile filenames

compile :: String -> IO ()
compile filename = do
  parseResult <- parse filename
  let compilationResult = do
        program <- parseResult
        _ <- R.typeProgram program
        let inlined = Semantics.inline program
        _ <- R.typeExpression (S.simplify S.absurdX inlined) >>= CT.checkPar mempty
        C.compile inlined
  case compilationResult of
    Failure _ -> hPutDocLn stderr (pretty compilationResult)
    Success x ->
      let outfile = filename -<.> ".pnkc"
      in withFile outfile WriteMode (\handle -> hPutDoc handle (pretty x))

parse :: String -> IO (Result (S.Program String String))
parse filename = do
  r <- parseFromFileEx P.program filename
  return $ case r of
    TriF.Success e -> Success e
    TriF.Failure (TriF.ErrInfo msg _) -> Failure msg

-- Local Variables:
-- dante-target: "plonkc"
-- End:

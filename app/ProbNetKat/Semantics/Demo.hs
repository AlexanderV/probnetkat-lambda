{- |

Module:      ProbNetKat.Semantics.Demo
Description: Useful functions for demoing the semantics.
Maintainer:  alexander.vandenbroucke@kuleuven.be

This module implements a number of useful functions to define demo programs for
PloNK.

 * 'load': load a PNK file, parse it and apply the semantics
 * 'integrateResult': integrate a function with respect to 'Histories' given in
   a 'Result'.

-}

module ProbNetKat.Semantics.Demo where

import           ProbNetKat.Result
import           ProbNetKat.Semantics (
  Histories, PacketHistory,Log,runE, modify, integrate,toList,historyLength)
import qualified ProbNetKat.Syntax.Parser as Parser

import           Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import qualified Data.HashMap.Strict as HashMap
import qualified Text.Trifecta.Parser as TriF
import qualified Text.Trifecta.Result as TriF

integrateResult
  :: Result Histories -> (HashSet PacketHistory -> Log) -> Result Double
integrateResult r f = fmap integrate' r where integrate' h = integrate h f

load
  :: String
  -> Int
  -> HashSet.HashSet PacketHistory
  -> IO (Result Histories)
load filename n phs = fmap head (loadRange filename n n phs)
  
loadRange
  :: String
  -> Int
  -> Int
  -> HashSet.HashSet PacketHistory
  -> IO [Result Histories]
loadRange filename low high phs = do
  parseResult <- TriF.parseFromFileEx Parser.expression filename
  return $ case parseResult of
    TriF.Success e -> [ runE n phs e | n <- [low..high]]
    TriF.Failure (TriF.ErrInfo msg _) -> [Failure msg]

(<~) :: String -> Integer -> PacketHistory -> PacketHistory
field <~ value = modify field value

type Query = HashSet.HashSet PacketHistory -> Log

bool2num :: Num a => Bool -> a
bool2num True  = 1
bool2num False = 0

trace :: [Integer] -> Query
trace t phs =
  let check p i = p HashMap.! "player" == i
  in bool2num (all (\ph -> and (zipWith check (toList ph) (reverse t))) phs)

maxLatency :: Query
maxLatency =
  let pathLength = fromIntegral . historyLength
  in HashSet.foldl' (\r ph -> max r (pathLength ph)) 0

meanLatency :: Query
meanLatency set =
  let n = HashSet.size set
      pathLength = fromIntegral . historyLength
  in if n > 0 then sum (HashSet.map pathLength set) / fromIntegral n else 0

congestion :: Integer -> Integer -> Query
congestion switch port set =
  let onLink p = p HashMap.! "sw" == switch && p HashMap.! "pt" == port
      countLink history = length [p | p <- toList history, onLink p]
  in sum (HashSet.map (fromIntegral . countLink) set)

infected :: Query
infected = fromIntegral . HashSet.size

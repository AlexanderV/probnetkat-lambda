import qualified Data.HashSet as HashSet

import           Control.Monad (zipWithM_)
import           System.Environment (getArgs)
import           System.IO (hPutStrLn,stderr)
import           Text.Pretty (putPrettyLn)

import           ProbNetKat.Result (Result(Success))
import           ProbNetKat.Semantics (blank, test, historyLength,integrate)
import           ProbNetKat.Semantics.Demo ((<~))
import qualified ProbNetKat.Semantics.Demo as Demo

main :: IO ()
main = do
  arguments <- getArgs
  case arguments of
    ["fault-tolerance"] -> faultTolerance
    
    ["load-balancing"] -> loadBalancing
    
    ["infected",filename,low,high] ->
      infected filename (read low) (read high)
    _ -> hPutStrLn stderr "semantics-demo: invalid arguments"

faultTolerance :: IO ()
faultTolerance = do
  m <- Demo.load "fault-tolerance.pnk" 4 (HashSet.map ("SW" <~ 1) blank)
  let integrator = Demo.bool2num . any (test "SW" 4) . HashSet.toList
  putStrLn ("Fault tolerance: " ++ show (Demo.integrateResult m integrator))

loadBalancing :: IO ()
loadBalancing = do
  m <- Demo.load "load-balancing.pnk" 1 blank
  let d = Demo.integrateResult m $ \s ->
        fromIntegral . maximum . map historyLength . HashSet.toList $ s
        -- let linkCounts (MkPH packet packets) =
        --       [(p ! "sw",p ! "pt") | p <- (packet:packets)]
        --     aggregate = map length .  group . sort
        -- in fromIntegral .
        --    maximum .
        --    aggregate .
        --    concat .
        --    map linkCounts .
        --    HashSet.toList $ s
  putStrLn ("Load balancing: " ++ show d)

infected :: String -> Int -> Int -> IO ()
infected filename low high = do
  let darkblock = toEnum 0x2588
      midblock = toEnum 0x2592
      lightblock = toEnum 0x2591
      bar maxn n = concat [
        replicate (n-1) midblock,
        [darkblock | n > 0],
        replicate (maxn-n) lightblock]
      forResult n (Success history) = do
        putStr (show n ++ ": ")
        let i = integrate history Demo.infected
        putStr (bar 16 (ceiling (2*i)) ++ " ")
        putPrettyLn i
      forResult n result = do
        putStr (show n ++ ": ")
        putPrettyLn (result >> return ())
  results <- Demo.loadRange filename low high blank
  putStrLn "Number of infected nodes after N rounds:"
  zipWithM_ forResult [low..high] results
      


-- leaders :: Int -> IO ()
-- leaders n = do
--   m <- load "leaders-lambda.pnk" n blank
--   putPrettyLn $ do
--     d <- integrateResult m (bool2num . not . HashSet.null)
--       -- Expected maximum latency given that the query succeeds
--     l <- integrateResult m (maxLatency)
--     return (d, l / d)


-- example :: Int -> IO ()
-- example n = do
--   let packet =
--         HashSet.map (modify "dst" 3 . modify "sw" 1 . modify "pt" 1) blank
--   m <- load "example.pnk" n packet
--   let links = [(1,2),(2,1),(2,3),(3,2),(3,4),(4,3),(4,1),(4,1)]
--   putPrettyLn $
--     fmap maximum [integrateResult m ( congestion h p) | (h,p) <- links]
--       -- print $ integrate m (bool2num . any (\ph -> test "sw" 3 ph && test "pt" 3 ph))


-- Local Variables:
-- dante-target: "semantics-demo"
-- End:

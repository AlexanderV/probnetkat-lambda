{-# LANGUAGE ViewPatterns #-}

import           Text.Trifecta.Parser (parseString,parseFromFileEx)
import           Text.Parser.Combinators (eof)
import qualified Text.Trifecta.Result as TriF
import           System.IO (hFlush,stdout)
import           System.Environment (getArgs)
import           ProbNetKat.Typing as T
import           ProbNetKat.Typing.Reconstruction
import qualified ProbNetKat.Syntax as S
import qualified ProbNetKat.Syntax.Parser as Parser
import qualified ProbNetKat.Syntax.Simple as Simple
import           Text.Pretty (Pretty(pretty),putPrettyLn,putDocLn)
import qualified Text.PrettyPrint.ANSI.Leijen as PP

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["check",filename] -> checkFile filename
    ["scheme",filename] -> schemeFile filename
    _ -> welcomeMessage >> loop

welcomeMessage :: IO ()
welcomeMessage =
  putStrLn
    "Hello and welcome to this computer-aided typing enrichment center activity."

withParseResult :: TriF.Result a -> (a -> IO ()) -> IO ()
withParseResult (TriF.Success x) k = k x
withParseResult (TriF.Failure (TriF.ErrInfo errDoc _)) _ =
  putDocLn (PP.pretty errDoc)


checkFile :: String -> IO ()
checkFile filename = do
  result <- parseFromFileEx Parser.program filename
  withParseResult result $ \p -> case typeProgram p of
    Success typed ->
      putDocLn $ PP.vsep [
      PP.blue (PP.string "Program"),
      PP.indent 2 (pretty typed) ]
    failed -> putPrettyLn failed

schemeFile :: String -> IO ()
schemeFile filename = do
  parseResult <- parseFromFileEx Parser.program filename
  withParseResult parseResult $ \program ->
    case schemeProgram program of
      Success (cs,schemed) ->
        putDocLn $ PP.vsep [
          PP.blue (PP.string "Constraints"),
          PP.indent 2 (pretty cs),
          PP.blue (PP.string "Program"),
          PP.indent 2 (pretty schemed)]
      failed -> putPrettyLn failed
    
loop :: IO ()
loop = do
 -- 0x03bb is the UTF-16 code for greek small lambda
  putStr (toEnum 0x03bb:"PNK> ")
  hFlush stdout
  str <- getLine
  case str of
    ":q" -> putStrLn "I don't hate you."
    (words -> (":l":filename:_)) -> do
        r <- parseFromFileEx Parser.program filename
        withParseResult r $ \program -> do
          putPrettyLn program
          printTypes program
        loop
    _  -> do
      let result = parseString (Parser.expression <* eof) mempty str
      withParseResult result (printTypes . S.MkProgram [])
      loop

printTypes
  :: (Pretty var, Eq var) => Simple.Program var var -> IO ()
printTypes program = do
  putStrLn "The expression is:"
  putDocLn $ PP.bold $ PP.indent 2 $ pretty $ S._main_expression program

  putStr "The scheme of this expression is: "
  putPrettyLn (schemeOf . S._main_expression . snd <$> schemeProgram program)
  putStrLn "With substitution:"
  putPrettyLn (fst <$> schemeProgram program)
  
  putStr "The type of this expression is: "
  putPrettyLn (typeOf . S._main_expression <$> typeProgram program)
  
  putStrLn "The scheme-annotated version of this expression is: "
  putDocLn $ PP.bold $ PP.indent 2 $ pretty (snd <$> schemeProgram program)


import qualified ProbNetKat.Semantics.Query as Q
import           ProbNetKat.Semantics.SmallStep
import           ProbNetKat.Syntax.Parser (expression)
import           System.IO (hFlush,stdout)
import           Text.Parser.Combinators (eof)
import           Text.Pretty (putPretty, putPrettyLn)
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import           Text.Trifecta.Parser (parseString)
import           Text.Trifecta.Result (Result(Success,Failure))

main :: IO ()
main = do
  let query = Q.Lambda (Q.MkVar "b") (Q.MaxLen (Q.Var (Q.MkVar "b")))
  putStrLn
    "Hello and welcome to this computer-aided lambda enrichment center activity."
  loop "b" query

loop :: String -> Query -> IO ()
loop var query = do
  putPretty query
  putStr "> "
  hFlush stdout
  str <- getLine
  case str of
    "" -> putStrLn "I don't hate you."
    _  -> case parseString (expression <* eof) mempty str of
      failure@(Failure _) -> print (PP.pretty failure) >> loop var query
      Success expr        -> do
        putPrettyLn $ Q.runFresh $ snd $ steps (wrap expr) (return query)
        loop var query
import           Text.Trifecta.Parser (parseFromFileEx)
import qualified Text.Trifecta.Result as TriF

import qualified Data.HashSet as HashSet
import           ProbNetKat.Semantics
import           ProbNetKat.Semantics.Demo
import qualified ProbNetKat.Syntax.Parser as Parser
import           Text.Pretty (putPrettyLn)

main :: IO ()
main = do
  result <- parseFromFileEx Parser.program "gossip-protocols.pnk"
  case result of
    TriF.Success program -> putPrettyLn $ do
      let m = runN 1 blank program
      integrateResult m (return . toDouble . any (test "SW" 0) . HashSet.toList)
    _ -> print result

{-# LANGUAGE BangPatterns #-}
import           Criterion.Main

import           ProbNetKat.Result
import           ProbNetKat.Semantics
import qualified ProbNetKat.Semantics.Compilation as C
import qualified ProbNetKat.Syntax.DeBruijn as DB
import qualified ProbNetKat.Syntax.Parser as Parser
import qualified ProbNetKat.Syntax.Simple as Simple

import           Control.DeepSeq as DS
import           Control.Monad ((>=>))
import           Data.HashSet (HashSet)
import qualified Text.Trifecta.Parser as TriF
import qualified Text.Trifecta.Result as TriF

main :: IO ()
main = defaultMain [
  env parseFile $ \e ->
      bgroup "gossip-protocols" [
          bgroup "no-determine" [
              bench "1" $ resultNF (\n -> runExprNoDetermine n blank e) 1,
              bench "2" $ resultNF (\n -> runExprNoDetermine n blank e) 2,
              bench "3" $ resultNF (\n -> runExprNoDetermine n blank e) 3,
              bench "4" $ resultNF (\n -> runExprNoDetermine n blank e) 4,
              bench "5" $ resultNF (\n -> runExprNoDetermine n blank e) 5,
              bench "6" $ resultNF (\n -> runExprNoDetermine n blank e) 6
              ]
            -- bgroup "determine" [
            --   bench "1" $ resultNF (\n -> runExprDetermine n blank e) 1,
            --   bench "2" $ resultNF (\n -> runExprDetermine n blank e) 2,
            --   bench "3" $ resultNF (\n -> runExprDetermine n blank e) 3,
            --   bench "4" $ resultNF (\n -> runExprDetermine n blank e) 4,
            --   bench "5" $ resultNF (\n -> runExprDetermine n blank e) 5,
            --   bench "6" $ resultNF (\n -> runExprDetermine n blank e) 6
            --   ]
          ]
  ]


resultNF :: NFData b => (a -> Result b) -> a -> Benchmarkable
resultNF f x = nf (resultNF' . f) x where
  resultNF' (Success a)    = force a
  resultNF' (Failure err) = error (show err)

parseFile :: IO (Simple.Expression String String)
parseFile = do
  let filename = "gossip-protocols.pnkc"  
  parseResult <- TriF.parseFromFileEx Parser.expression filename
  case parseResult of
    TriF.Failure (TriF.ErrInfo msg _) -> error (show msg)
    TriF.Success e -> return e

-- | Run n-th approximation of an expression with respect to a set of histories.
runExprNoDetermine
  :: Int
  -> HashSet PacketHistory
  -> Simple.Expression String String
  -> Result Histories
runExprNoDetermine n ph = C.erase . DB.addDeBruijn [] >=> evalE n ph

-- -- | Run n-th approximation of an expression with respect to a set of histories.
-- runExprDetermine
--   :: Int
--   -> HashSet PacketHistory
--   -> Simple.Expression String String
--   -> Result Histories
-- runExprDetermine n ph = C.erase . DB.addDeBruijn [] >=> evalE n ph . C.determine
